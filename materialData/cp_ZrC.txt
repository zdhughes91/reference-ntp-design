(51.027+3.685*(temperature/1000)-0.199*(temperature/1000)^2+0.028*(temperature/1000)^3-1.304/(temperature/1000)^2) * 1000/103.2347
298 362.5155 3805 616.2629
Notes:
Temperature Units: [K]
Specific Heat Units: [J/(kg-K)]
Original Equation:
51.027+3.685*(temperature/1000)-0.199*(temperature/1000)^2+0.028*(temperature/1000)^3-1.304/(temperature/1000)^2
Convert from [J/(mol-K)] to [J/(kg-K)]
Molar Masses
Zr 91.224
C 12.0107
Combined Molar Mass
103.2347
Multiplier
1/103.2347 mol/g * 1000 g/kg
Bounds are from 298 to 3805, the data fit approaches negative infinity quickly under 298K. Note that t = T/1000
REFERENCE: Harrison, R. W., and W. E. Lee. "Processing and Properties of ZrC, ZrN and ZrCN Ceramics: A Review." Advances in Applied Ceramics 115, no. 5 (2016): 294-307. 
LINK: https://doi.org/10.1179/1743676115Y.0000000061.
ADDITIONAL: https://webbook.nist.gov/cgi/cbook.cgi?ID=C12070143&Units=SI&Mask=2#Thermo-Condensed




