# =================================================================================== #
# =================================================================================== #
# ====================== -- Summer 2023 Numerical Modeling -- ======================= #
# ======================      --  Ultra Safe Nuclear  --     ======================== #
# =================================================================================== #
# ========================== Zach Hughes - zhughes@tamu.edu ========================= #
# Files:                                                                              #
# 1. materialScript.ipynb: this contains a table showing all sources and methods for  #
#    creating thermal conductivity equations of state. Also contains UZrC's method    #
# 2. fittingScript.py: contains the solution for thermal conductivty equations of     #
#    state for porousZrC, denseZrC,Be                                                 #
# 3. polynomialPlotter.ipynb: contains all equations of state solutiosn for liqH      #
# ----------------------------------------------------------------------------------- #
# -- To Do:                                                                           #
#                                                                                     #
# ----------------------------------------------------------------------------------- #
# Sources:                                                                            #
# =================================================================================== #
