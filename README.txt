# =================================================================================== #
# =================================================================================== #
# ====================== -- Summer 2023 Numerical Modeling -- ======================= #
# ======================      --  Ultra Safe Nuclear  --     ======================== #
# =================================================================================== #
# ========================== Zach Hughes - zhughes@tamu.edu ========================= #
# Folders:                                                                            #
# 1. fuelAssemblyCases: This contains the heterogenous and porous-media cases of the  #
#    assembly only.                                                                   #
# 2. Literature: This folder contains papers as well as textbooks that were used in   #
#    the process of building all of the models. Some give material data others give   #
#    equations, etc.                                                                  #
# 3. unitCellCases: This contains heterogenous and porous-media cases of the entire   #
#    unit cell.                                                                       #
# 4. Raptor: Contains everything that has to do with the Raptor model as well as the  #
#    Serpent models                                                                   #
# 5. materialData: Contains all scripts that were used to calculate equations of state#
#    as well as organizes all material sources/methods in one location                #
# ----------------------------------------------------------------------------------- #
# To Do:                                                                              #
#                                                                                     #
# ----------------------------------------------------------------------------------- #
# Sources:                                                                            #
# =================================================================================== #
