import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import os
#import seaborn as sns

class postProcess:
    def __init__(self):
        # below is the link where i found teh reynolds correlation for friction factor
        # https://www.sciencedirect.com/science/article/pii/S0017931011000500?ref=pdf_download&fr=RR-2&rr=7eceb23e1907eb47
        self.cList = {'c':'r', # list of colors, where creativity can shine
                    '1':'g',
                    '2':'b',
                    '3':'r',
                    '4':'g',
                    '5':'b'}
        
        hd = '/home/grads/z/zhughes/Summer2023/fuelAssemblyCases/'
        
        ringCdata = pd.read_csv(hd+'finalResults/data/heterogenousData/cccData.csv')
        ring1data = pd.read_csv(hd+'finalResults/data/heterogenousData/icc1Data.csv')
        ring2data = pd.read_csv(hd+'finalResults/data/heterogenousData/icc3Data.csv')
        ring3data = pd.read_csv(hd+'finalResults/data/heterogenousData/icc6Data.csv')
        ring4data = pd.read_csv(hd+'finalResults/data/heterogenousData/icc10Data.csv')
        ring5data = pd.read_csv(hd+'finalResults/data/heterogenousData/icc15Data.csv')
        structureData = pd.read_csv(hd+'finalResults/data/heterogenousData/structDataMax.csv')
        ringCCladdata = pd.read_csv(hd+'finalResults/data/heterogenousData/cccCladData.csv')
        ring1Claddata = pd.read_csv(hd+'finalResults/data/heterogenousData/icc1CladData.csv')
        ring2Claddata = pd.read_csv(hd+'finalResults/data/heterogenousData/icc3CladData.csv')
        ring3Claddata = pd.read_csv(hd+'finalResults/data/heterogenousData/icc6CladData.csv')
        ring4Claddata = pd.read_csv(hd+'finalResults/data/heterogenousData/icc10CladData.csv')
        ring5Claddata = pd.read_csv(hd+'finalResults/data/heterogenousData/icc15CladData.csv')
        
        ringCdataPM = pd.read_csv(hd+'finalResults/data/porousMedia_6ringsWithHeatDiffusion/genfoam_cccData.csv')
        ring1dataPM = pd.read_csv(hd+'finalResults/data/porousMedia_6ringsWithHeatDiffusion/genfoam_ring1data.csv')
        ring2dataPM = pd.read_csv(hd+'finalResults/data/porousMedia_6ringsWithHeatDiffusion/genfoam_ring2data.csv')
        ring3dataPM = pd.read_csv(hd+'finalResults/data/porousMedia_6ringsWithHeatDiffusion/genfoam_ring3data.csv')
        ring4dataPM = pd.read_csv(hd+'finalResults/data/porousMedia_6ringsWithHeatDiffusion/genfoam_ring4data.csv')
        ring5dataPM = pd.read_csv(hd+'finalResults/data/porousMedia_6ringsWithHeatDiffusion/genfoam_ring5data.csv')
        
        ringCdataPMnoHD = pd.read_csv(hd+'finalResults/data/porousMedia_6ringsWithoutHeatDiffusion/ringCData_6ringsWithoutHD.csv')
        ring1dataPMnoHD = pd.read_csv(hd+'finalResults/data/porousMedia_6ringsWithoutHeatDiffusion/ring1Data_6ringsWithoutHD.csv')
        ring2dataPMnoHD = pd.read_csv(hd+'finalResults/data/porousMedia_6ringsWithoutHeatDiffusion/ring2Data_6ringsWithoutHD.csv')
        ring3dataPMnoHD = pd.read_csv(hd+'finalResults/data/porousMedia_6ringsWithoutHeatDiffusion/ring3Data_6ringsWithoutHD.csv')
        ring4dataPMnoHD = pd.read_csv(hd+'finalResults/data/porousMedia_6ringsWithoutHeatDiffusion/ring4Data_6ringsWithoutHD.csv')
        ring5dataPMnoHD = pd.read_csv(hd+'finalResults/data/porousMedia_6ringsWithoutHeatDiffusion/ring5Data_6ringsWithoutHD.csv')
        
        self.porousMedia_noRingsData = pd.read_csv(hd+'finalResults/data/porousMedia_NoRings/genfoamNoRingsData.csv')

        self.coolantChannelData = {'c':ringCdata, # putting all coolant channel data into one variable
                                   '1':ring1data,
                                   '2':ring2data,
                                   '3':ring3data,
                                   '4':ring4data,
                                   '5':ring5data}
        self.structureData={'struct':structureData, # putting all solid data into one variable
                            'c':ringCCladdata,
                            '1':ring1Claddata,
                            '2':ring2Claddata,
                            '3':ring3Claddata,
                            '4':ring4Claddata,
                            '5':ring5Claddata}
        self.porousMediaData = {'c':ringCdataPM, # putting all genfoam data into one variable
                                '1':ring1dataPM,
                                '2':ring2dataPM,
                                '3':ring3dataPM,
                                '4':ring4dataPM,
                                '5':ring5dataPM}
        self.porousMediaDataWithoutHD = {'c':ringCdataPMnoHD, # 
                                         '1':ring1dataPMnoHD,
                                         '2':ring2dataPMnoHD,
                                         '3':ring3dataPMnoHD,
                                         '4':ring4dataPMnoHD,
                                         '5':ring5dataPMnoHD}
    
    def getRelativeDifference(self,item1,item2):
        """
        Returns the percent relative difference between item1 and item2 
        """
        relativeDifference  = 1e2 * ((item1-item2) / item1)
        return relativeDifference
    
    def plotCCtemp(self,close=False):
        plt.figure(1,dpi=200)
        for cc in self.coolantChannelData:
            tempRelativeDiff = self.getRelativeDifference(self.coolantChannelData[cc]['T'],self.porousMediaData[cc]['T']) 
            plt.plot(self.coolantChannelData[cc]['arc_length'],tempRelativeDiff,label='Ring '+cc)
        plt.legend(loc='best')
        plt.title('Relative Difference: Coolant Channel Temperatures')
        plt.ylabel('Percent Difference %')
        plt.xlabel('Height (m)')
        plt.grid(True)
        #plt.savefig(hd+'finalResults/pictures/compareCoolantTemp1.png')
        if not close: plt.show()
    
    def plotCCpres(self,close=False):
        plt.figure(2,dpi=200)
        for cc in self.coolantChannelData:
            presRelativeDiff = self.getRelativeDifference(1e-6*self.coolantChannelData[cc]['p_rgh'],1e-6*self.porousMediaData[cc]['p_rgh']) 
            plt.plot(self.porousMediaData[cc]['arc_length'],presRelativeDiff,label='Ring '+cc)
        plt.legend(loc='best')
        plt.title('Relative Difference: Coolant Channel Pressures')
        plt.ylabel('Percent Difference %')
        plt.xlabel('Height (m)')
        plt.grid(True)
        #plt.savefig(hd+'finalResults/pictures/comparePressure1.png')
        if not close: plt.show()
    
    def plotCCvelo(self,close=False):
        plt.figure(3,dpi=200)
        for cc in self.coolantChannelData:
            veloRelativeDiff = self.getRelativeDifference(np.abs(self.coolantChannelData[cc]['U:2']),np.abs(self.porousMediaData[cc]['U:2']))
            plt.plot(self.coolantChannelData[cc]['arc_length'],veloRelativeDiff,label='Ring '+cc)
        plt.legend(loc='best')
        plt.title('Relative Difference: Coolant Channel Z-Velocity')
        plt.ylabel('Percent Difference %')
        plt.xlabel('Height (m)')
        plt.grid(True)
        #plt.savefig(hd+'finalResults/pictures/compareVelocity1.png')
        if not close: plt.show()

    def plotStructTemp(self,close=False):
        plt.figure(4,dpi=200)
        plt.plot(self.porousMediaData['5']['arc_length'],self.porousMediaData['5']['Tmax.lumpedNuclearStructure'],label='GeN-Foam: Tmax',linestyle='dashed')
        #plt.plot(self.structureData['pm']['arc_length'],self.structureData['pm']['Tsurface.lumpedNuclearStructure'],label='GeN-Foam: Tsurf',linestyle='dashed')
        plt.plot(self.structureData['struct']['arc_length'],self.structureData['struct']['T'],label='multiRegion OD')
        plt.legend(loc='best')
        plt.title('Fuel Assembly Structure Temperature')
        plt.ylabel('Temperature (K)')
        plt.xlabel('Height (m)')
        plt.grid(True)
        #plt.savefig(hd+'finalResults/pictures/compareStructure.png')
        if not close: plt.show()
    
    # def plotCladTemps(self,close=False):
    #     plt.figure(6,dpi=200)
    #     for cc in self.structureData:
    #         if cc == "struct": continue
    #         cladRelativeDiff = self.getRelativeDifference(self.structureData[cc]['T'],self.porousMediaData[cc]['T.cladAvForNeutronics'])
    #         plt.plot(self.structplotStructTemperence: Cladding Temperatures')
    #     plt.ylabel('Percent Difference %')
    #     plt.xlabel('Height (m)')
    #     plt.grid(True)
    #     #plt.savefig(hd+'finalResults/pictures/compareCladStructure.png')
    #     if not close: plt.show()
    
    def getDensity(self,T):
        rho = 6.19235776 - 1.63183743e-03 * T # taken from genfoamCase/constant/fluidRegion/thermophysicalProperties to see density axially
        return rho
    
    def buildGenfoamDensities(self):
        "GeN-Foam is not returning axial densities so I am finding it myself based on the equation of state it was given"
        for cc in self.porousMediaData:
            self.porousMediaData[cc]['rho'] = self.getDensity(self.porousMediaData[cc]['T'])
        
    #def plotDensities(self):
    #    fig, ax = plt.subplots()
    #def plotForPresentation(self):
        # pct_diff = np.abs(100*(self.porousMedia_noRingsData['Tmax.lumpedNuclearStructure'] - self.structureData['struct']['T'])/ self.structureData['struct']['T'])
        # self.structureData['struct']['pct_diff_present'] = pct_diff
        # sns.set(style='darkgrid',palette='muted')
        # sns.lineplot(x='arc_length', y='Tmax.lumpedNuclearStructure', data=self.porousMedia_noRingsData,label='Homog. PM')
        # sns.lineplot(x='arc_length', y='T', data=self.structureData['struct'],label='Hetero')
        # plt.title('Max Temperature Distribution')
        # plt.xlabel('Height (m)')
        # plt.ylabel('Temperature (K)')
        # plt.legend()
        # #ax2.figure.legend()
        # plt.show()
    def compareModelTemps(self):
        plt.figure(7)
        plt.plot(self.structureData['struct']['arc_length'],self.structureData['struct']['T'],label='Hetero Fuel',c='r')
        plt.plot(self.porousMedia_noRingsData['arc_length'],self.porousMedia_noRingsData['Tmax.lumpedNuclearStructure'][::-1],label='PM Fuel Single Region',c='b')
        plt.plot(self.porousMediaDataWithoutHD['5']['arc_length'],self.porousMediaDataWithoutHD['5']['Tmax.lumpedNuclearStructure'],label='PM Fuel 6 Regions w/o Heat Diffusion',c='g')
        plt.plot(self.porousMediaData['5']['arc_length'],self.porousMediaData['5']['Tmax.lumpedNuclearStructure'],label='PM Fuel 6 Regions w/ Heat Diffusion',c='m')
        
        plt.plot(self.coolantChannelData['5']['arc_length'],self.coolantChannelData['5']['T'],label='Hetero Fluid T',c='r',linestyle='dashed')
        plt.plot(self.porousMedia_noRingsData['arc_length'],self.porousMedia_noRingsData['T'][::-1],label='PM Fluid Single Region',c='b',linestyle='dashed')
        plt.plot(self.porousMediaDataWithoutHD['5']['arc_length'],self.porousMediaDataWithoutHD['5']['T'],label='PM Fluid w/o Heat Diffusion',c='g',linestyle='dashed')
        plt.plot(self.porousMediaData['5']['arc_length'],self.porousMediaData['5']['T'],label='PM Fluid w/ Heat Diffusion',c='m',linestyle='dashed')
        plt.legend(loc='best')
        plt.title('Maximum Fuel Assembly Temperatures')
        plt.xlabel('Height (m)')
        plt.ylabel('Temperature (K)')
        plt.show()
    
    def getTemperatureChanges(self):
        delThetero = self.structureData['struct']['T'] - self.coolantChannelData['5']['T']
        delTpmNoRings = self.porousMedia_noRingsData['Tmax.lumpedNuclearStructure'][::-1] - self.porousMedia_noRingsData['T'][::-1]
        delTpm6RingsNoHD = self.porousMediaDataWithoutHD['5']['Tmax.lumpedNuclearStructure'] - self.porousMediaDataWithoutHD['5']['T']
        delTpm6RingsYesHD = self.porousMediaData['5']['Tmax.lumpedNuclearStructure'] - self.porousMediaData['5']['T']
        
        self.relateNoRings = self.getRelativeDifference(delTpmNoRings,delThetero)
        self.relate6ringsNoHD = self.getRelativeDifference(delTpm6RingsNoHD,delThetero)
        self.relate6ringsYesHD = self.getRelativeDifference(delTpm6RingsYesHD,delThetero)
        
    def plotRelativeDifference(self):
        self.getTemperatureChanges()
        plt.figure(8)
        plt.plot(self.structureData['struct']['arc_length'],self.relateNoRings,label='PM Single Region')
        plt.plot(self.structureData['struct']['arc_length'],self.relate6ringsNoHD,label='PM w/o Heat Diffusion')
        plt.plot(self.structureData['struct']['arc_length'],self.relate6ringsYesHD,label='PM w/ Heat Diffusion')
        plt.title('Relative Differences')
        plt.xlabel('Height (m)')
        plt.ylabel('Relative Difference (%)')
        plt.legend(loc='best')
        plt.show()
        
        
    def plotAll(self,close=False):
        self.plotCCtemp(close)
        self.plotCCpres(close)
        self.plotCCvelo(close)
        self.plotStructTemp(close)
        #self.plotCladTemps(close)

if __name__ == '__main__':

    genfoam = postProcess()
    #genfoam.plotCCtemp()
    #genfoam.plotCCpres()
    genfoam.compareModelTemps()
    genfoam.plotRelativeDifference()
    #genfoam.plotAll()
    #genfoam.plotForPresentation()