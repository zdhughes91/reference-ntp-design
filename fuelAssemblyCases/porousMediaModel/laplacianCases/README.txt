# =================================================================================== #
# =================================================================================== #
# =================== Reference NTP Design laplacianFoam Models ===================== #
# ======================      -- First Working Model --      ======================== #
# =================================================================================== #
# ========================= Zach Hughes - zhughes@tamu.edu  ========================= #
# Notes:                                                                              #
# -- The model will run by typing './Allrun' from the 'laplacianCases' folder. It     #
#    should take a few seconds to run.                                                #
# -- The purpose of this folder is to solve 2D heat conduction in each of the six     #
#    rings so that heat conductances can be found from the surface temperature to the #
#    average temperature as well as from the average temperature to the max           #
#    temperature. Theses heat conductances are used in the GeN-Foam model             #
# -- Each folder contains a 2D model of a different ring of the fuel assembly. Ring1  #
#    is the center ring and ring6 is the outermost ring.                              #
# --One python script calculates the useful value                                     #
#     2. /porousMediaModel/laplacianCases/script.py                                   #  
#      - calculates the fvOptions input for each of the 5 rings                       #   
# ----------------------------------------------------------------------------------- #
# -- To Do:                                                                           #
#                                                                                     #
# ----------------------------------------------------------------------------------- #
# Material Sources:                                                                   #
# -- UZrC Properties                                                                  #
# https://www.sciencedirect.com/science/article/pii/S2352179122001715#s0040           #
# =================================================================================== #

