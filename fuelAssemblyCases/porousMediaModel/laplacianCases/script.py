import numpy as np


class preProcess:
    def __init__(self):
        self.ringHeight = 0.0001 # m, height of all rings in salome
        # below is ring1,ring2,ring3,ring4,ring5,ring6. all from salome calculations
        self.ringAreas = [2.49735e-5,0.000180079,0.000351631,0.00052311,0.000694692,0.00111052] # m^2
        self.ringVolumes = [2.49735e-9,1.80077e-08,3.51615e-08,5.23107e-08,6.94686e-08,1.11052e-07] # m^3
        self.rho_UZrC = 5.458e3 # g/m^3
        self.Cp_UZrC = 500 # J/(kg K)
        self.corePower = 225e6 # W
        self.assemblyPower = self.corePower / 61 # 61 assemblies in the core
        self.assemblyVolume = np.pi * (0.0635/2)**2 * 1.1 # m^3

    def get_fvOptionsT(self):
        self.specificPower = 1.16e9 # W/m^3
        self.fvOptionsT = self.specificPower / (self.rho_UZrC * self.Cp_UZrC)
        print('fvOptionsT = {:0.6f} K/m^3 '.format(self.fvOptionsT))

class postProcess:
    def __init__(self):
        self.ringHeight = 0.0001
        # volume is volume of structure excluding coolant channels, ccArea is coolant channel heated area. numC is number of coolant channels
        self.geometricData = {'ring1':{'faceArea':2.49735e-5, 'volume':2.49735e-9, 'ccArea':6.25112e-07,'numC':1}, # all of this data came from salome's measurement feature
                              'ring2':{'faceArea':0.000180079,'volume':1.80077e-08,'ccArea':3.75443e-06,'numC':6},
                              'ring3':{'faceArea':0.000351631,'volume':3.51615e-08,'ccArea':7.50885e-06,'numC':12},
                              'ring4':{'faceArea':0.00052311, 'volume':5.23107e-08,'ccArea':1.12633e-05,'numC':18},
                              'ring5':{'faceArea':0.000694692,'volume':6.94686e-08,'ccArea':1.50177e-05,'numC':24},
                              'ring6':{'faceArea':0.00111052, 'volume':1.11052e-07,'ccArea':1.87721e-05,'numC':30},
                              'clad':{'faceArea':1.73896e-06}}
        ring1data = np.genfromtxt('ring1/0.5/T',skip_header=23,skip_footer=243-216)
        ring2data = np.genfromtxt('ring2/0.5/T',skip_header=23,skip_footer=1546-1515)
        ring3data = np.genfromtxt('ring3/0.5/T',skip_header=23,skip_footer=2878-2847)
        ring4data = np.genfromtxt('ring4/0.5/T',skip_header=23,skip_footer=4319-4288)
        ring5data = np.genfromtxt('ring5/0.5/T',skip_header=23,skip_footer=5768-5737)
        ring6data = np.genfromtxt('ring6/0.5/T',skip_header=23,skip_footer=7879-7848)
        
        self.data = {'ring1':ring1data,
                     'ring2':ring2data,
                     'ring3':ring3data,
                     'ring4':ring4data,
                     'ring5':ring5data,
                     'ring6':ring6data}
        
        self.specificPower = 1.165e9
        # below is a list of sources for the thermophysical properties data --------------------------------------
        # ZrC-Hf:k   = W/(m*K)   https://www.osti.gov/biblio/4003326
        # ZrC-Hf:Cp  =  J/kg*K), https://www.sciencedirect.com/science/article/abs/pii/S0022311513007824
        # ZrC-Hf:rho =  kg/m^3 , from baseline.mater
        # UZrC:k     =  W/(m*K)  at 1500 K https://www.sciencedirect.com/science/article/pii/S2352179122001715#s0040 --> paragraph below Fig.12
        # UZrC:Cp    =  J/(kg*K) at 2500 K https://www.sciencedirect.com/science/article/pii/S2352179122001715#s0040 --> Fig.9
        # UZrC:rho   =  based on Performance of (U,Zr)C-graphite (composite) and of (U,Zr)C (carbide) fuel elements in the Nuclear Furnace 1 test reactor
        # above is a list of sources for the thermophysical properties data --------------------------------------
        self.thermophysicalProperties = {'ZrC-Hf':{'k':35,'Cp':500,'rho':6.73029e3},
                                         'UZrC':{'k':15.5,'Cp':500,'rho':5.458e3}}       

    def findTemperatures(self):
        self.tempPoints = {} # will contain a list for each ring. The list contains [Taverage,Tmax]
        for ring in self.data:
            Tav = np.mean(self.data[ring])
            Tmax = np.max(self.data[ring])
            self.tempPoints[ring] = [Tav,Tmax]
    
    def getHeatConductance(self,Ts):
        "Ts should be the fixedValue temperature boundary condition given in the laplacianFoam models"
        self.findTemperatures()
        self.conductances = {}
        cladThickness = 0.00025 # m
        print('='*10+'heat conductances'+'='*10)
        heatedAreaTOT = 91 * 1.1 * 2 * np.pi * 0.001 # = nChannels * h * circumference
        self.volStructure = (1.1*np.pi*(0.03175)**2) - (91*1.1*np.pi*(0.001**2))
        print('vs',self.volStructure)
        HcladTOTAL = self.thermophysicalProperties['ZrC-Hf']['k'] * heatedAreaTOT / (cladThickness * self.volStructure)
        print('HcladTotal is for a genfoam model with no rings!')
        print('HcladTotal = {:0.4e} W/(m^3 K)'.format(HcladTOTAL))
        for ring in self.tempPoints:
            heatedArea = self.geometricData[ring]['ccArea'] * 1.1 / self.ringHeight
            Hclad = self.thermophysicalProperties['ZrC-Hf']['k'] * heatedArea / (cladThickness * (self.geometricData[ring]['volume'] * 1.1/self.ringHeight))
            H_sToAv = self.specificPower / (self.tempPoints[ring][0] - Ts)
            H_avToMax = self.specificPower / (self.tempPoints[ring][1] - self.tempPoints[ring][0])
            self.conductances[ring] = [H_sToAv,H_avToMax]
            print(ring+'H = ({:0.4e} {:0.4e} {:0.4e}) W/(m^3 K)'.format(H_avToMax,H_sToAv,Hclad))
        print('='*35)
        
    def getVolumetricAreas(self):
        "gets volumetric area input for genfoam"
        print('='*10+'volumetric areas'+'='*10)
        for ring in self.geometricData:
            if ring == 'clad': continue
            self.geometricData[ring]['volArea'] = self.geometricData[ring]['ccArea'] / self.geometricData[ring]['volume']
            print(ring+': volArea = {:0.3f} 1/m '.format(self.geometricData[ring]['volArea']))
        print('='*35)
    
    def getVolumeFraction(self):
        channelArea = np.pi * (0.001**2) # radius of channel is .1mm
        print('='*10+'volume fraction'+'='*10)
        for ring in self.geometricData:
            if ring == 'clad': continue
            totalVolume = self.geometricData[ring]['volume'] + (self.geometricData[ring]['numC'] * channelArea * self.ringHeight)
            self.geometricData[ring]['volFrac'] = self.geometricData[ring]['volume'] / totalVolume
            print(ring+': volFrac = {:0.4f} '.format(self.geometricData[ring]['volFrac']))
        print('volfracTOT for 0 ring model!')
        volfracClad = (91 * ((np.pi*0.00125**2)-(np.pi*0.001**2)) * 1.1) / self.volStructure
        volfracFuel = 1 - volfracClad
        print('volumeFractions ({:0.4f} {:0.4f}) '.format(volfracFuel,volfracClad))
        print('='*35)

    def getRhoCp(self):
        print('='*10+'   rhoCp values   '+'='*10)
        for mat in self.thermophysicalProperties:
            print(mat+': rhoCp = {:0.4e} '.format(self.thermophysicalProperties[mat]['rho']*self.thermophysicalProperties[mat]['Cp']))
        print('='*35)

    def getVolumeFractions(self):
        self.geometricData['clad']['volume'] = self.geometricData['clad']['faceArea'] * self.ringHeight
        print('='*10+' volumeFractions values   '+'='*10)
        for ring in self.geometricData:
            if ring == 'clad': continue 
            totVol = self.geometricData[ring]['volume'] + (self.geometricData[ring]['numC']*self.geometricData['clad']['volume'])
            print(ring+': volumeFractions ({:0.4f} {:0.4f})'.format(self.geometricData[ring]['volume']/totVol,(self.geometricData[ring]['numC']*self.geometricData['clad']['volume'])/totVol))
        print('='*35)

if __name__ == '__main__':

    rings = preProcess()
    rings.get_fvOptionsT()

    genfoamstuff = postProcess()
    genfoamstuff.getHeatConductance(300)
    genfoamstuff.getVolumetricAreas()
    genfoamstuff.getVolumeFraction()
    genfoamstuff.getRhoCp()
    genfoamstuff.getVolumeFractions()