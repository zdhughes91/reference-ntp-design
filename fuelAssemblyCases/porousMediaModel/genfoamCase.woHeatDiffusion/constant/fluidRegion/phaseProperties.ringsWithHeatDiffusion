/*--------------------------------*- C++ -*----------------------------------*\
| =========                 |                                                 |
| \\      /  F ield         | OpenFOAM: The Open Source CFD Toolbox           |
|  \\    /   O peration     | Version:  v2112                                 |
|   \\  /    A nd           | Website:  www.openfoam.com                      |
|    \\/     M anipulation  |                                                 |
\*---------------------------------------------------------------------------*/
FoamFile
{
    version     2.0;
    format      ascii;
    class       dictionary;
    location    "constant/fluidRegion";
    object      phaseProperties;
}
// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

// ------------------------------------------------------------------------- //
// --- THERMAL-HYDRAULIC TYPE ---------------------------------------------- //
// ------------------------------------------------------------------------- //

thermalHydraulicsType    "onePhase";

// ------------------------------------------------------------------------- //
// --- STRUCTURES PROPERTIES ----------------------------------------------- //
// ------------------------------------------------------------------------- //
// the calculated values all come from /sixRings/laplacianCases/script.py
structureProperties 
{
    ring1 // innermost ring
    {
        volumeFraction      0.8883; 
        Dh                  0.002; 
        powerModel
        {
            type                lumpedNuclearStructure; 
            volumetricArea      250.310; // 1/m  , 
            powerDensity        1.165e9; // W/(m^3)
            nodesNumber         2; 
            nodeFuel            0; 
            nodeClad            0; 
            heatConductances    (1.8452e+07 7.2326e+06 3.5043e+07); 
            rhoCp               (2.7290e+06 3.3651e+06); //
            volumeFractions     (0.9349 0.0651); 
            powerFractions      (1 0); 
            T0                  300; 
            kappaMatrix         15.5;
            nodeMatrix		0;
        }
    }
    ring2 //
    {
        volumeFraction      0.9052; 
        Dh                  0.002; 
        powerModel
        {
            type                lumpedNuclearStructure; 
            volumetricArea      208.490; // 1/m  , 
            powerDensity        1.165e9; // W/(m^3)
            nodesNumber         2; 
            nodeFuel            0; 
            nodeClad            0; 
            heatConductances    (5.4083e+06 5.4510e+06 2.9189e+07); 
            rhoCp               (2.7290e+06 3.3651e+06); //
            volumeFractions     (0.9452 0.0548); 
            powerFractions      (1 0); 
            T0                  300; 
            kappaMatrix         15.5;
            nodeMatrix		0;
        }
    }
    ring3 // 
    {
        volumeFraction      0.9032; 
        Dh                  0.002; 
        powerModel
        {
            type                lumpedNuclearStructure; 
            volumetricArea      213.553; // 1/m  , 
            powerDensity        1.165e9; // W/(m^3)
            nodesNumber         2; 
            nodeFuel            0; 
            nodeClad            0; 
            heatConductances    (6.1091e+06 5.8326e+06 2.9897e+07); 
            rhoCp               (2.7290e+06 3.3651e+06); //
            volumeFractions     (0.9440 0.0560); 
            powerFractions      (1 0); 
            T0                  300; 
            kappaMatrix         15.5;
            nodeMatrix		0;
        }
    }
    ring4 // 
    {
        volumeFraction      0.9024; 
        Dh                  0.002; 
        powerModel
        {
            type                lumpedNuclearStructure; 
            volumetricArea      215.315; // 1/m  , 
            powerDensity        1.165e9; // W/(m^3)
            nodesNumber         2; 
            nodeFuel            0; 
            nodeClad            0; 
            heatConductances    (4.7423e+06 5.4772e+06 3.0144e+07); 
            rhoCp               (2.7290e+06 3.3651e+06); //
            volumeFractions     (0.9435 0.0565); 
            powerFractions      (1 0); 
            T0                  300; 
            kappaMatrix         15.5;
            nodeMatrix		0;
        }
    }
    ring5 // 
    {
        volumeFraction      0.9021; 
        Dh                  0.002; 
        powerModel
        {
            type                lumpedNuclearStructure; 
            volumetricArea      216.180; // 1/m  , 
            powerDensity        1.165e9; // W/(m^3)
            nodesNumber         2; 
            nodeFuel            0; 
            nodeClad            0; 
            heatConductances    (6.6570e+06 5.9465e+06 3.0265e+07); 
            rhoCp               (2.7290e+06 3.3651e+06); //
            volumeFractions     (0.9433 0.0567); 
            powerFractions      (1 0); 
            T0                  300; 
            kappaMatrix         15.5;
            nodeMatrix		0;
        }
    }
    ring6 // outermost ring
    {
        volumeFraction      0.9218 ;
        Dh                  0.002;
        powerModel
        {
            type                lumpedNuclearStructure; 
            volumetricArea      169.039; // 1/m  , 
            powerDensity        1.165e9; // W/(m^3)
            nodesNumber         2; 
            nodeFuel            0; 
            nodeClad            1; 
            heatConductances    (3.0559e+06 3.9696e+06 2.3665e+07); 
            rhoCp               (2.7290e+06 3.3651e+06); //
            volumeFractions     (0.9551 0.0449); 
            powerFractions      (1 0); 
            T0                  300; 
            kappaMatrix         15.5;
            nodeMatrix		0;
        }
    }
}

// ------------------------------------------------------------------------- //
// --- REGIME MAP MODEL ---------------------------------------------------- //
// ------------------------------------------------------------------------- //

regimeMapModel
{
    type    none;
}

// ------------------------------------------------------------------------- //
// --- REGIME PHYSICS FOR EACH REGIME -------------------------------------- //
// ------------------------------------------------------------------------- //

physicsModels
{
    dragModels
    {
        ring1
        {
            type    ReynoldsPower; // 
            // 0.316* Re^-0.25 blasius correlation is valid for 3000 < Re < 2e4. 
            // 0.184 * Re^-0.2 blasius correlation is valid for 2e4 < Re < 2e6. 
            coeff   0.184; // 
            exp     -0.2; 
            //type    Colebrook;
            //coeff   0.79;
            //const   -1.64;
            //exp     -2.0;
            //type                Churchill;
            //surfaceRoughness    1e-6;
        }
        ring2
        {
            ${ring1}
        }
        ring3
        {
            ${ring1}
        }
        ring4
        {
            ${ring1}
        }
        ring5
        {
            ${ring1}
        }
        ring6
        {
            ${ring1}
        }
    }

    heatTransferModels
    {
        ring1
        {
            type    NusseltReynoldsPrandtlPower;
            const   0;
            coeff   0.023;
            expRe   0.8;
            expPr   0.4;
        }
        ring2
        {
            ${ring1}
        }
        ring3
        {
            ${ring1}
        }
        ring4
        {
            ${ring1}
        }
        ring5
        {
            ${ring1}
        }
        ring6
        {
            ${ring1}
        }
    }
}

// ------------------------------------------------------------------------- //
// --- MISCELLANEA --------------------------------------------------------- //
// ------------------------------------------------------------------------- //

pMin                    10000;
pRefCell                0;
pRefValue               1e5;



// ************************************************************************* //
