/*--------------------------------*- C++ -*----------------------------------*\
|       ______          _   __           ______                               |
|      / ____/  ___    / | / /          / ____/  ____   ____ _   ____ ___     |
|     / / __   / _ \  /  |/ /  ______  / /_     / __ \ / __ `/  / __ `__ \    |
|    / /_/ /  /  __/ / /|  /  /_____/ / __/    / /_/ // /_/ /  / / / / / /    |
|    \____/   \___/ /_/ |_/          /_/       \____/ \__,_/  /_/ /_/ /_/     |
|    Copyright (C) 2015 - 2022 EPFL                                           |
\*---------------------------------------------------------------------------*/
FoamFile
{
    version     2.0;
    format      ascii;
    class       dictionary;
    object      controlDict;
}
// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

application     GeN-Foam;

startFrom       startTime;

startTime       0;

stopAt          endTime;

endTime         33;

deltaT          0.001; 

writeControl    adjustableRunTime;

writeInterval   11;

purgeWrite      0;

writeFormat     ascii;

writePrecision  6;

writeCompression off;

timeFormat      general;

timePrecision   6;

runTimeModifiable true;

//- Physics to Solve

solveFluidMechanics     true;

solveEnergy             true;

solveNeutronics         false;

solveThermalMechanics   false;

//- Global options

liquidFuel          false;

//- Time step control options

adjustTimeStep      true;

maxDeltaT           1;

maxCo               50;

// ************************************************************************* //
// ************************************************************************* //

/*/*functions
{
    mFlowOutlet
    {
        type            massFlow;
        libs            ( "libfieldFunctionObjects.so" );
        log             true;
        writeFields     true;
        region          "fluidRegion";
        regionType      patch;
        regionName      "outlet";
        alphaRhoPhiName "alphaRhoPhi";
    }
    mFlowInlet
    {
        type            massFlow;
        libs            ( "libfieldFunctionObjects.so" );
        log             true;
        writeFields     true;
        region          "fluidRegion";
        regionType      patch;
        regionName      "inlet";
        alphaRhoPhiName "alphaRhoPhi";
    }

    /*heatTransferCoeff1
    {
        // Mandatory entries (unmodifiable)
        type            heatTransferCoeff;
        libs            ("libfieldFunctionObjects.so");
        field           T;
        patches         ("walls");
        htcModel        ReynoldsAnalogy;
        UInf            (0 0 63.6493);
        //writeFields     true;
        executeControl  adjustableRunTime;
        executeInterval 1;
        writeControl    adjustableRunTime;
        writeInterval   1;
        //Cp              CpInf;
        //CpInf           16000;
        region          "fluidRegion";
        rho             rhoInf;
        rhoInf          0.7;
    }*/
    /*heatTransferCoeff2
    {
        // Mandatory entries (unmodifiable)
        type            heatTransferCoeff;
        libs            ("libfieldFunctionObjects.so");
        field           T;
        patches         ("walls");
        htcModel        localReferenceTemperature;
        UInf            (0 0 63.6493);
        //Cp              CpInf;
        //CpInf           16000;
        region          "fluidRegion";
        rho             rhoInf;
        rhoInf          0.7;
    }*/
    /*heatTransferCoeff3
    {
        // Mandatory entries (unmodifiable)
        type            heatTransferCoeff;
        libs            ("libfieldFunctionObjects.so");
        field           T;
        patches         ("walls");
        htcModel        fixedReferenceTemperature;
        UInf            (0 0 63.6493);
        //Cp              CpInf;
        //CpInf           16000;
        region          "fluidRegion";
        rho             rhoInf;
        rhoInf          0.7;
        TRef            300;
    }*/
//}