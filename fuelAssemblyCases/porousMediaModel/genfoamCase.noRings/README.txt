# =================================================================================== #
# =================================================================================== #
# ====================== Reference NTP Design GeN-Foam Model ======================== #
# ======================      -- First Working Model --      ======================== #
# =================================================================================== #
# ========================= Zach Hughes - zhughes@tamu.edu  ========================= #
# Notes:                                                                              #
# -- The model will run by typing GeN-Foam from the rootCase, and should take 5-10min #
# -- The genfoam model was split into six evenly spaced rings so the model could      #
#    better describe the temperature gradients that occur in the fuel assembly        #
# -- Three python scripts exist that calculate all useful values:                     #
#     1. /heterogenousModel/preProcessHetCase.py                                      #
#      -calculates geometric,thermo,turbulence, OpenFOAM input values                 #
#     2. /porousMediaModel/laplacianCases/script.py                                   #
#      -calculates all genfoam inputs for each of the six rings                       #
#      - calcualtes rootCase/constant/fluidRegion/phaseProperties inputs              #
#     3. /porousMediaModel/genfoamCase/polynomialPlotter.ipynb                        #
#      -finds lines of best fit for all hydrogen thermophysical properties, which are #
#       used in genfoam in rootCase/constant/fluidRegion/thermophysicalProperties     #   
# -- The constant/fluidRegion/ folder has the phaseProperties files that were used to #
#    collect data on the porous6rings with heat diffusion, porous6rings without heat  #
#    diffusion, and porous0rings. The porous0rings also used a different mesh, which  #
#    exists in the same folder as this README, named 'genfoamNoRingsMesh.unv'. By     #
#    changing these files (as well as the mesh for porous0rings) the results can be   #
#    repeated.                                                                        #
# -- The mesh for this case was created by using the system/createBafflesDict file.   #
#    The mesh can be recreated by using '$ ideasUnvToFoam genfoam6ringsMesh.unv' and  #
#    'createBaffles'. This is needed for separating the rings in genfoam              #
# ----------------------------------------------------------------------------------- #
# -- To Do:                                                                           #
#                                                                                     #
# ----------------------------------------------------------------------------------- #
# Material Sources:                                                                   #
# -- Hydrogen Properties                                                              #
# https://webbook.nist.gov/cgi/fluid.cgi?P=3.7&TLow=80&THigh=1000&TInc=10&Digits=5&ID=C1333740&Action=Load&Type=IsoBar&TUnit=K&PUnit=MPa&DUnit=kg%2Fm3&HUnit=kJ%2Fkg&WUnit=m%2Fs&VisUnit=Pa*s&STUnit=N%2Fm&RefState=DEF
# ^ CoolProp Python API was also used to access NIST database for liqH ^              #
# -- UZrC Properties                                                                  #
# https://www.sciencedirect.com/science/article/pii/S2352179122001715#s0040           #
# https://www.osti.gov/biblio/4419566                                                 #
# 2. ZrC kappa : https://www.osti.gov/biblio/4003326                                  #
# 3. ZrC Cp: https://www.sciencedirect.com/science/article/abs/pii/S0022311513007824  #
# =================================================================================== #

