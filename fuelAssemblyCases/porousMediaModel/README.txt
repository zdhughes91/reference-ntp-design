# =================================================================================== #
# =================================================================================== #
# ======================= GeN-Foam and laplacianFoam 6-Ring model =================== #
# =================================================================================== #
# ========================= Zach Hughes - zhughes@tamu.edu  ========================= #
# Folders:                                                                            #
# 1. genfoamCase: Contains a genfoam case with the six coolant channel rings split    #
#    apart. To run, just type "GeN-Foam"                                              #
# 2. laplacianCases: Contains a laplacianFoam case for each ring in the fuel assembly.#
#    These can be run simultaneously using the the "./Allrun" command. All post       #
#    processing for this case (which is also preprocessing for the genfoam case) is   #
#    done in the script.py python file within this folder. These cases were necessary #
#    to solve for the heat conductances within each ring                              #
# ----------------------------------------------------------------------------------- #
# TO DO:                                                                              #
# 1.                                                                                  #
# ----------------------------------------------------------------------------------- #
# Citations:                                                                          #
# 2. ZrC kappa : https://www.osti.gov/biblio/4003326                                  #
# 3. ZrC Cp: https://www.sciencedirect.com/science/article/abs/pii/S0022311513007824  #
# 4. UZrC: https://www.sciencedirect.com/science/article/pii/S2352179122001715#s0040  #
# =================================================================================== #
# =================================================================================== #
