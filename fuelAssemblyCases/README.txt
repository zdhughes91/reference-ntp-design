# =================================================================================== #
# =================================================================================== #
# =================== Reference NTP Design Fuel Assembly Model ====================== #
# ======================      -- First Working Model --      ======================== #
# =================================================================================== #
# ========================= Zach Hughes - zhughes@tamu.edu  ========================= #
# Folders:                                                                            #
# 1. finalResults: Contains .csv files with all the output data from the genfoam and  #
#    heterogenous models. More details about these .csv files are contained in the    #
#    README file located in the finalResults folder. Plots of the output data are     #
#    also located here.                                                               #
# 2. heterogenousModel: Contains a chtMultiRegionSimpleFoam model that is used for    #
#    verification of the genfoam model. There is also a python script located here    #
#    that solves for various OpenFOAM input values for both the chtMultiRegionFoam    #
#    case as well as for the GeN-Foam case                                            #
# 3. porousMediaModel: Contains the GeN-Foam model of the fuel assembly as well as    #
#    a 2D laplacianFoam model that was used to find heat conductances within each     #
#    ring of the fuel assembly. A few python scripts exist in this folder and are     #
#    described in more detail in the README file located in these folders             #
# Files:                                                                              #
# 1. plotResulsScript.py: This file imports the .csv files in finalResults folder     #
#    and plots them in a way that is useful. These plots are then saved into the      #
#    finalResults/pictures/ folder                                                    #
# ----------------------------------------------------------------------------------- #
# -- To Do:                                                                           #
#                                                                                     #
# ----------------------------------------------------------------------------------- #
# Sources:                                                                            #
# -- Hydrogen Properties                                                              #
# 1. https://webbook.nist.gov/cgi/fluid.cgi?P=3.7&TLow=80&THigh=1000&TInc=10&Digits=5&ID=C1333740&Action=Load&Type=IsoBar&TUnit=K&PUnit=MPa&DUnit=kg%2Fm3&HUnit=kJ%2Fkg&WUnit=m%2Fs&VisUnit=Pa*s&STUnit=N%2Fm&RefState=DEF
# ^ CoolProp Python API was also used to access NIST database for liqH ^              #
# -- UZrC Properties                                                                  #
# 2. https://www.sciencedirect.com/science/article/pii/S2352179122001715#s0040        #
# 3. https://www.osti.gov/biblio/4419566                                              #
# 4. ZrC kappa : https://www.osti.gov/biblio/4003326                                  #
# 5. ZrC Cp: https://www.sciencedirect.com/science/article/abs/pii/S0022311513007824  #
# =================================================================================== #

