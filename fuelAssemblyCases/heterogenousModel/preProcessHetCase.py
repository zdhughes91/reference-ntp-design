import numpy as np
import os
import pandas as pd
import matplotlib.pyplot as plt


# >>> parameters >>>
totalPower = 225 # MWt
coolChannelDiameter = 0.2 # cm
coolCladThickness = 0.025  # cm
assemblyCanThickness = 0.025 # cm
insulatorThickness = 0.2 # cm
gasGapThickness = 0.3 #cm
assemblySpacing = 2.5 # cm - spacing between assembly ODs
nCoolChannels = 91
nAssemblies = 61
fuelAssemblyDiameter = 6.35 # cm
coreActiveHeight = 110 #cm
openFoamDiskHeight = 0.0001 #m
Ts = 500 # K
#coolantInletVelocity = 47.6 # m/s
inletPressure = 7 # MPa
coreMassFlowrate = 6.33 # kg/s


rho_UZrC = 5.458e3 # based on Performance of (U,Zr)C-graphite (composite) and of (U,Zr)C (carbide) fuel elements in the Nuclear Furnace 1 test reactor
rho_liqH = 5.702806 # kg/m^3 from NIST using CoolProp (@ 300K and 7.3MPa)
Cp_UZrC = 500 # J/(kg*K) at 2500 K https://www.sciencedirect.com/science/article/pii/S2352179122001715#s0040 --> Fig.9
Cp_liqH = 14310.12686 # J/(kg*K) from NIST using CoolProp (@ 300K and 7.3MPa)
k_UZrC = 15.5 # W/(m*K)  at 1500 K https://www.sciencedirect.com/science/article/pii/S2352179122001715#s0040 --> paragraph below Fig.12
#k_UZrC = 30   # W/(m*K) at 2600 K https://www.osti.gov/servlets/purl/4419566 --> table CVIII
mu_liqH = 1.2739e-5 # Pa*s    Tis is 
k_ZrCHf = 5 # W/(m*K)   at 1200 K  https://par.nsf.gov/servlets/purl/10285629 --> Fig.7D
Cp_ZrCHf = 362 # J/kg*K), found from wikipedia for ZrC

tempFilename = os.getcwd() + f"/0.1/T"
# <<< parameters <<<
class unitCell:
    def __init__(self,assemblyRadius,canThickness,insulThickness,gasThickness,moderThickness):
        self.cm2m = 1e-2
        self.assemblyRadius = assemblyRadius * self.cm2m
        self.canThickness = canThickness * self.cm2m
        self.insulThickness = insulThickness * self.cm2m
        self.gasThickness = gasThickness * self.cm2m
        self.moderThickness = moderThickness * self.cm2m

        self.totalPower = 225e6 # W
        self.rhoUZrC = 5.458e3 # kg/m^3
        self.CpUZrC = 500 #J/kg K
    def getRadialDimensions(self):
        self.assemblyCanOR = self.assemblyRadius + self.canThickness
        self.insulatorOR = self.assemblyCanOR + self.insulThickness
        self.gasGapOR = self.insulatorOR + self.gasThickness
        self.moderatorOR = self.insulatorOR + self.moderThickness
    def printRadialDimensions(self):
        #getRadialDimensions(self)
        print('---------------- Radial Dimensions ----------------')
        print('Fuel Assembly OR:',0.03175,'m')
        print('Assembly Can OR :',np.round(self.assemblyCanOR,5),'m')
        print('Insulator OR    :',np.round(self.insulatorOR,5),'m')
        print('Gas Gap OR      :',np.round(self.gasGapOR,5),'m')
        print('Moderator OR    :',np.round(self.moderatorOR,5),'m')
        print('----------------------------------------------------')
    # 'i' functions are for internal use only
    def icircularArea(self,r): 
        return np.pi*(r**2)
    def ivolumetricARea(self):
        perimeter = 2 * np.pi * self.coolantChannelRadius
        height = 1.1
        singleCoolantChannelArea = perimeter * height
        totatCoolantChannelArea = 91 * singleCoolantChannelArea
        assemblyArea = self.icircularArea(self.assemblyRadius)
        assemblyVolume = assemblyArea * height
        print('volumetric area')
        print('including coolant channels = {:0.4f}'.format(totatCoolantChannelArea/assemblyVolume))
    def imultiRegionVAandPoro(self):
        sliceArea = 0.000697124
        sliceVolume = sliceArea * 1.1
        totalCoolantChannelArea = 21 * self.icircularArea(self.coolantChannelRadius)
        totalCoolantChannelHeatedArea = 21 * 2 * np.pi * self.coolantChannelRadius * 1.1
        totalCoolantChannelVolume = totalCoolantChannelArea * 1.1
        multiRegionVolumetricArea = totalCoolantChannelHeatedArea / sliceVolume
        print('multiRegion volumetricArea = {:0.4f} '.format(multiRegionVolumetricArea))
        porosity = (sliceVolume - totalCoolantChannelVolume) / sliceVolume
        print('multiRegion Porosity = {:0.4f} '.format(porosity))

    def iInletVelocity(self):
        # currently not using gas gap area because I am not sure if its included in the mass flowrate
        inletArea =  61 * (91 * self.icircularArea(self.coolantChannelRadius)) #+  61 * (self.icircularArea(self.gasGapOR) - self.icircularArea(self.insulatorOR)) )
        self.inletVelocity = (self.massFlowrate / (inletArea*self.rhoLiqH)) #* 0.85 # 0.85 is to adjust for developing flow
        
    def iReynolds(self,Dh):
        return self.rhoLiqH*self.inletVelocity*Dh/self.muLiqH
    def iL(self): 
        # capital L (characteristicLength) is the equivalent diameter according to 'intro to CFD:  Versteeg and Malalasekera'
        self.ccL = 0.07*self.coolantChannelDh
        self.ggL = 0.07*self.gasGapDh
    def iTurbulenceIntensity(self):
        self.ccI = 0.16 * self.ccRe**(-1/8)
        self.ggI = 0.16 * self.ggRe**(-1/8)
    def iTurbulentKineticEnergy(self):
        self.cck = (3/2) * (self.ccI*self.inletVelocity)**2
        self.ggk = (3/2) * (self.ggI*self.inletVelocity)**2
    def iTurbulentDissapationEnergy(self):
        self.ccEpsilon = (0.09**(0.75)) * (self.cck**1.5) / self.ccL 
        self.ggEpsilon = (0.09**(0.75)) * (self.ggk**1.5) / self.ggL 
    def iMolarMass(self): # g/mol
        self.mmUZrC = 238 + 91.22 + 12.011 
        self.mmZrCHf = 91.22 + 12.011 + 178.5 
        self.mmZrC07Hf = 0.7*(91.22 + 12.011) + 178.5
        self.mmliqH = 2.0
    def iAssemblySlicePower(self): # 
        fuelAssemblyArea = self.icircularArea(self.assemblyRadius) - (91 * self.icircularArea(self.coolantChannelRadius))
        self.fuelAssemblyVolume = fuelAssemblyArea * 1.1 # m^3
        self.fuelAssemblyMass = self.fuelAssemblyVolume * self.rhoUZrC #kg
        self.assemblyPower = (self.totalPower/61) 
        self.sliceAbsolutePower = self.assemblyPower / 6 # because i took a 60degree slice
    def ifvOptionsEnthalpy(self):
        self.fvOptionsSpecificEnthalpy = self.assemblyPower / (self.fuelAssemblyMass * self.fuelAssemblyVolume)  # W/(m^3 kg)
        self.fvOptionsAbsoluteEnthalpy = self.assemblyPower / self.fuelAssemblyMass  # W/kg
    def getLaplacianfvOptionsT(self):
        "returns the temperature input for the fvOptions internal heat generation for (DT,T) laplacian"
        T = self.assemblyPower / (self.CpUZrC * self.rhoUZrC * self.fuelAssemblyVolume)
        print('-- fvOptions T (volumeMode=specific)')
        print('T =',T,'K','\n')
        Tspec = T * (self.fuelAssemblyVolume / 6) # dividing by 6 sice I took 60degree slice
        print('-- fvOptions T (volumeMode=absolute)')
        print('T =',Tspec,'K','\n')
        return T

    def getDh(self,coolChannelRadius):
        self.coolantChannelRadius = coolChannelRadius * self.cm2m
        Pcc = (2 * np.pi * self.coolantChannelRadius) # coolant channel 
        Acc = self.icircularArea(self.coolantChannelRadius)
        self.coolantChannelDh = 4*Acc/Pcc

        Pgg = (2*np.pi*self.gasGapOR) + (2*np.pi*self.insulatorOR) # gas gap
        Agg = self.icircularArea(self.gasGapOR) - self.icircularArea(self.insulatorOR)
        self.gasGapDh = 4*Agg/Pgg

    def getRe(self,rhoLiqH,muLiqH,massFlowrate):
        self.rhoLiqH = rhoLiqH
        self.muLiqH = muLiqH
        self.massFlowrate = massFlowrate
        self.iInletVelocity()
        self.ccRe = self.iReynolds(self.coolantChannelDh)
        self.ggRe = self.iReynolds(self.gasGapDh)

    def getTurbulenceBCs(self):
        self.iL() 
        self.iTurbulenceIntensity()
        self.iTurbulentKineticEnergy()
        self.iTurbulentDissapationEnergy()

    def printMolarMasses(self):
        self.iMolarMass()
        print('---------------- Molar Masses ----------------')
        print('UZrC      = {:0.3f} g/mol '.format(self.mmUZrC))
        print('ZrC-HF    = {:0.3f} g/mol '.format(self.mmZrCHf))
        print('ZrC0.7-Hf = {:0.3f} g/mol '.format(self.mmZrC07Hf))
        print('liqH      = {:0.3f} g/mol '.format(self.mmliqH))
        print('----------------------------------------------')
        
    def printfvOptionsEnthalpy(self):
        self.ifvOptionsEnthalpy()
        print('h_specific = {:0.5f} W/(m^3 kg) '.format(self.fvOptionsSpecificEnthalpy))
        print('h_absolute = {:0.5f} W/kg '.format(self.fvOptionsAbsoluteEnthalpy))
        self.getLaplacianfvOptionsT()
    def printOutletMassflow(self,density,velocity):
        print("--- Outlet Mass Flowrate from Results: ---")
        print("MFR = {:0.5f} kg/s ".format(density*velocity*self.icircularArea(self.assemblyRadius)))




if __name__ == '__main__':
    unit = unitCell(fuelAssemblyDiameter/2,assemblyCanThickness,insulatorThickness,gasGapThickness,assemblySpacing/2)
    unit.getRadialDimensions(), unit.printRadialDimensions()
    unit.getDh(coolChannelDiameter/2)#, print('dhgg = {:.4f} m'.format(unit.gasGapDh))


    unit.getRe(rho_liqH,mu_liqH,coreMassFlowrate)

    print('Re_cc = {:0.4f} '.format(unit.ccRe))
    print('Re_gg = {:0.4f} '.format(unit.ggRe))
    print('Uinlet = {:0.4f} m/s'.format(unit.inletVelocity))

    unit.getTurbulenceBCs()
    print('Lcc,Lgg = {:0.6f},{:0.6f}'.format(unit.ccL,unit.ggL))
    print('Icc,Igg = {:0.4f},{:0.4f}'.format(unit.ccI,unit.ggI))
    print('kcc,kgg = {:0.4f},{:0.4f}'.format(unit.cck,unit.ggk))
    print('Epcc,Epgg = {:0.4f},{:0.4f}'.format(unit.ccEpsilon,unit.ggEpsilon))

    #unit.printMolarMasses()
    #unit.printSlicePower()
    #unit.printfvOptionsEnthalpy()
    unit.ivolumetricARea()
    unit.imultiRegionVAandPoro()
    #unit.printExpectedAvgTemp()