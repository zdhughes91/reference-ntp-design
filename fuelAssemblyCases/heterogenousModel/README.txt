# =================================================================================== #
# =================================================================================== #
# ==================== Reference NTP Design Heterogenous Model ====================== #
# ======================      -- First Working Model --      ======================== #
# =================================================================================== #
# ========================= Zach Hughes - zhughes@tamu.edu  ========================= #
# Notes:                                                                              #
# -- The model will run by typing './Allrun'. You can also run in parallel using      #
#    './Allrun.quick'. In parallel it should take around 3hrs to run                  #
# -- This case uses chtMultiRegionSimpleFoam to model a 60 degree slice of a fuel     #
#    assembly. It has 43 regions total to model each of the coolant channels as well  #
#    as the cladding and fuel assembly.                                               #
# -- Two python scripts exist that calculate all useful values:                       #
#     1. /heterogenousModel/preProcessHetCase.py                                      #
#      -calculates geometric,thermo,turbulence, OpenFOAM input values                 #     
#     2. /porousMediaModel/polynomialPlotter.ipynb                                    #
#      -calculates all lines of best fit for hydrogen thermophysicalProperteis        #     
# ----------------------------------------------------------------------------------- #
# -- To Do:                                                                           #
#                                                                                     #
# ----------------------------------------------------------------------------------- #
# Material Sources:                                                                   #
# -- Hydrogen Properties                                                              #
# https://webbook.nist.gov/cgi/fluid.cgi?P=3.7&TLow=80&THigh=1000&TInc=10&Digits=5&ID=C1333740&Action=Load&Type=IsoBar&TUnit=K&PUnit=MPa&DUnit=kg%2Fm3&HUnit=kJ%2Fkg&WUnit=m%2Fs&VisUnit=Pa*s&STUnit=N%2Fm&RefState=DEF
# ^ CoolProp Python API was also used to access NIST database for liqH ^              #
# -- UZrC Properties                                                                  #
# https://www.sciencedirect.com/science/article/pii/S2352179122001715#s0040           #
# https://www.osti.gov/biblio/4419566                                                 #
# 2. ZrC kappa : https://www.osti.gov/biblio/4003326                                  #
# 3. ZrC Cp: https://www.sciencedirect.com/science/article/abs/pii/S0022311513007824  #
# =================================================================================== #

