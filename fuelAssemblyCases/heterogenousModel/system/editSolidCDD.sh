#!/bin/bash
# --------------------------------------------- #
# purpose of this is to copy the ccc file and   #
# paste to each other coolant channel folder    #
# while changing the appropriate boundaries     #
# --------------------------------------------- #
# --- user input options
# base thermophysicalProperties:
base_cdd="cccClad"
# Directory where the files are located
directory="/home/zhughes/projects/GeN-Foam/usnc/cladMultiRegion/system"

# Find all files named 'thermophysicalProperties' within the directory and its subdirectories
files=$(find "$directory" -type f -name 'changeDictionaryDict')

subdirectories=()
for file in $files; do
  # Get the directory name containing the file
  subdirectory=$(dirname "$file")
  subdirectory_name=$(basename "$subdirectory")
  if [[ "$subdirectory_name" == "$base_cdd" ]]; then # i dont want to loop thru the thermophysicalProperties im editing
    continue
  fi 
  if [[ "$subdirectory_name" == "FA" ]]; then # i dont want to loop thru the thermophysicalProperties of solid
    continue
  fi 
  if [[ "$subdirectory_name" == "ccc" ]]; then # i dont want to loop thru ccc
    continue
  fi 
  if [[ $subdirectory_name =~ [0-9]$ ]]; then
    echo "$subdirectory_name was skipped bc it is not a solid"
    # Add your 'continue' command here
    continue
  fi
  #echo $subdirectory_name #prints coolant channel name
  #echo $file #prints full file path
  
  rm $file # removing the old thermophysicalProperties
  cp "$directory/$base_cdd/changeDictionaryDict" "$subdirectory/$(basename "$file")" # pasting new base_cdd thermophysicalProperties

  sed -i "s/$base_cdd/$subdirectory_name/g" "$file"  # replaces every occurance of base_cdd with subdirectory name
  echo "Coolant Chanel: $subdirectory_name has been updated"
  
  #if [[ "$subdirectory_name" == "icc17f" ]]; then
  #  break
  #fi

done

