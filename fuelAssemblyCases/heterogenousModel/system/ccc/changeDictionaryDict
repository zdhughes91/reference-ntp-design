/*--------------------------------*- C++ -*----------------------------------*\
| =========                 |                                                 |
| \\      /  F ield         | OpenFOAM: The Open Source CFD Toolbox           |
|  \\    /   O peration     | Version:  v2212                                 |
|   \\  /    A nd           | Website:  www.openfoam.com                      |
|    \\/     M anipulation  |                                                 |
\*---------------------------------------------------------------------------*/
FoamFile
{
    version     2.0;
    format      ascii;
    class       dictionary;
    object      changeDictionaryDict;
}
// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

U
{
    internalField   uniform (0 0 -67.7);

    boundaryField
    {
    ccc
    {
        type            inletOutlet;
        inletValue      uniform (0 0 0);
        value           uniform (0 0 0);
    }
    ccc_top
    {
        type            fixedValue;
        value           uniform (0 0 -67.7);
    }
    ccc_to_cccClad
    {
        type            fixedValue;
        value           uniform (0 0 0);
    }
    ".*"
    {
        type            zeroGradient;
    }
    }
}

T
{
    internalField   uniform 300;

    boundaryField
    {
        ccc
        {
            type            inletOutlet;
            value           uniform 300;
            inletValue      uniform 300;
        }
        ccc_top
        {
            type            fixedValue;
            value           uniform 300;
        }
        ccc_to_cccClad
        {
            type            compressible::turbulentTemperatureRadCoupledMixed;
            Tnbr            T;
            kappaMethod     fluidThermo;
            value           uniform 300;
        }
        ".*"
        {
            type            zeroGradient;
        }
    }
}

epsilon
{
    internalField   uniform 37498.7028;

    boundaryField
    {
        ccc
        {
            type            zeroGradient;
        }
        ccc_top
        {
            type            turbulentMixingLengthDissipationRateInlet;
            mixingLength    0.00014; // calculated in mrf2DheatConduction.py
            value           $internalField;
        }
        ccc_to_cccClad
        {
            type            epsilonWallFunction;
            value           $internalField;
        }
        ".*"
        {
            type            zeroGradient;
        }
    }
}

k
{
    internalField   uniform 10.07;

    boundaryField
    {
        ccc
        {
            type            zeroGradient;
        }
        ccc_top
        {
            type            turbulentIntensityKineticEnergyInlet;
            intensity       0.040705; // *100, /100 do not change anything
            value           $internalField;
        }
        ccc_to_cccClad
        {
            type            kqRWallFunction;
            value           $internalField;
        }
        ".*"
        {
            type            zeroGradient;
        }
    }
}

p_rgh
{
    internalField   uniform 7.2e6;

    boundaryField
    {
        ccc
        {
            type            fixedValue;
            value           uniform 7e6;
        }

        ccc_top
        {
            type            fixedFluxPressure;
            value           $internalField;
        }

        ccc_to_cccClad
        {
            type            fixedFluxPressure;
            value           $internalField;
        }
        ".*" // this may be more stable as fixedfluxpressure...
        {
            type            zeroGradient;
        }
    }
}

p
{
    internalField   uniform 7.2e6;

    boundaryField
    {
        ccc
        {
            type            calculated;
            value           $internalField;
        }

        ccc_top
        {
            type            calculated;
            value           $internalField;
        }

        ccc_to_cccClad
        {
            type            calculated;
            value           $internalField;
        }
        ".*"
        {
            type            calculated;
            value           $internalField;
        }
    }
}

nut
{
    internalField   uniform 0;

    boundaryField
    {
        ccc
        {
            type            calculated;
            value           uniform 0;
        }
        ccc_top
        {
            type            calculated;
            value           uniform 0;
        }
        ccc_to_cccClad
        {
            type            nutkWallFunction;
            value           uniform 0;
        }
        ".*"
        {
            type            calculated;
            value           uniform 0;
        }
    }
}

alphat
{
    internalField   uniform 0;

    boundaryField
    {
        ccc
        {
            type            calculated;
            value           $internalField;
        }
        ccc_top
        {
            type            calculated;
            value           $internalField;
        }
        ccc_to_cccClad
        {
            type            compressible::alphatWallFunction;
            value           $internalField;
        }
        ".*"
        {
            type            calculated;
            value           uniform 0;
        }
    }
}
// ************************************************************************* //
