#!/bin/bash
# --------------------------------------------- #
# purpose of this is to copy the ccc file and   #
# paste to each other coolant channel folder    #
# while changing the appropriate boundaries     #
# --------------------------------------------- #
# --- user input options
# base thermophysicalProperties:
base_cdd="ccc"
# Directory where the files are located
directory="/home/zhughes/projects/GeN-Foam/usnc/cladMultiRegion/constant"

# Find all files named 'thermophysicalProperties' within the directory and its subdirectories
files=$(find "$directory" -type f -name 'thermophysicalProperties')

subdirectories=()
for file in $files; do
  # Get the directory name containing the file
  subdirectory=$(dirname "$file")
  subdirectory_name=$(basename "$subdirectory")
  if [[ "$subdirectory_name" == "$base_cdd" ]]; then # i dont want to loop thru the thermophysicalProperties im editing
    continue
  fi 
  if [[ "$subdirectory_name" == "FAf" ]]; then # i dont want to loop thru the thermophysicalProperties of solid
    continue
  fi 
  #echo $subdirectory_name #prints coolant channel name
  #echo $file #prints full file path
  
  rm $file # removing the old thermophysicalProperties
  cp "$directory/$base_cdd/thermophysicalProperties" "$subdirectory/$(basename "$file")" # pasting new base_cdd thermophysicalProperties

  sed -i "s/$base_cdd/$subdirectory_name/g" "$file"  # replaces every occurance of base_cdd with subdirectory name
  echo "Coolant Chanel: $subdirectory_name has been updated"
  
  #if [[ "$subdirectory_name" == "icc17f" ]]; then
  #  break
  #fi

done

