# =================================================================================== #
# =================================================================================== #
# ======================= Reference NTP Design Output Data  ========================= #
# ======================      -- First Working Model --      ======================== #
# =================================================================================== #
# ========================= Zach Hughes - zhughes@tamu.edu  ========================= #
# Notes:                                                                              #
# --                                                                                  #
# Folders:                                                                            #
# 1. data: Contains .csv files that were taken from the output data of both the       #
#    hetergoenous case and the GeN-Foam case.                                         #
#     a. data/heteroGenousData/:                                                      #
#     - Here there is data taken from ParaView using the                              #
#     'plot over line' command. For the coolant channel data, these were taken near   #
#     the coolant channel centerlines for coolant channels ccc, icc1, icc3, icc6,     # 
#     icc10, icc15. Note that each of these coolant channels are cut off at the wall, #
#     so only 50% of these coolant channels are actually modelled. The cladding data  #
#     was taken at the claddings for those same coolant channels, at about the halfway#
#     point (at half thickness in clad and on the side of cladding facing +y). The    #
#     structure data was taken right before the assembly outer diameter on the +x     #
#     axis.                                                                           #
#     b. data/porousMedia_6ringsWithHeatDiffusion/:                                   #
#     - Here there is data taken from Paraview using the                              #
#     'plot over line' command. For this, one .csv was taken axially in each of the   #
#     six rings defined in GeN-Foam. This data was taken by allowing each of the      #
#     rings to communicate in genfoam. This is done by adding the 'kappaMatrix' and   #
#     'nodeMatrix' arguments to each of the rings in the phaseProperties file.        #
#     c. data/porousMedia_6ringsWithHeatDiffusion/:                                   #
#     - This folder contains data that was made with the 'kapaMatrix' and 'nodeMatrix'# 
#      arguments absent in the phaseProperties file. The data was taken from ParaView #
#      in the same way as (b.)                                                        #
#     d. data/porousMedia_NoRings/:                                                   #
#     - This folder contains data made with no rings, and without heat diffusion.     #
#      the data was taken from ParaView with the 'plot over line' command through the #
#      origin of assembly since it is uniform radially.                               #
# 2. pictures: Contains output plots comparing the heterogenous and porous media      #
#    cases                                                                            #
# ----------------------------------------------------------------------------------- #
# -- To Do:                                                                           #
#                                                                                     #
# ----------------------------------------------------------------------------------- #

