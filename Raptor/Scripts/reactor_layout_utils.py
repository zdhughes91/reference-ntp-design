import math
import numpy as np
import itertools
from typing import List, Tuple

MAX_STORED_RINGS = 100
ITEMS_PER_HEX_RING = [6 * i for i in range(MAX_STORED_RINGS)]
ITEMS_PER_HEX_RING[0] = 1
ALL_HEX_RING_POS = []
for i in range(MAX_STORED_RINGS):
    for j in range(ITEMS_PER_HEX_RING[i]):
        ALL_HEX_RING_POS.append((i + 1, j + 1))


def getAssemblyHexArraySize(
    assemblyPitch: float, assemblyGap: float, outerDiameterToFitInto: float
) -> Tuple[int, int]:
    """
    Calculate optimal core-wide hex lattice size to fill cylinder completely.

    Parameters
    ----------
    assemblyPitch : float
         Pitch between two FE, not including the gap
    assemblyGap : float
        Assembly gap
    outerDiameterToFitInto : float
        Diameter of the circle this the lattice must fit into

    Returns
    -------
    hexArraySize : int
        The core-wide assembly hex lattice size such that there are
        hexArraySize**2 lattice elements.
    nLatticeHexRings : int
        The number of hex rings in the lattice
    """

    hexPitch = assemblyPitch + assemblyGap * 2
    hexEdgeLength = hexPitch / math.sqrt(3)

    nLatticeHexRings = 1 + math.ceil(
        (outerDiameterToFitInto / 2)
        / ((hexPitch - hexEdgeLength / 2) / math.cos(math.pi / 6))
    )
    hexArraySize = (nLatticeHexRings - 1) * 2 + 1

    return hexArraySize, nLatticeHexRings


def getNumberOfHexPositionsInFullRings(rings: float) -> int:
    """
    Number of hex positions in specified lattice rings.

    For example, this function returns [1,7,19] for rings = [1,2,3].

    Parameters
    ----------
    rings : int
        The number of hex assembly rings (assuming that all rings are "full")

    Returns
    -------
    sum(jvals[0:rings]) : int
        The total number of hex assemblies in "full" rings.
    """
    return sum(ITEMS_PER_HEX_RING[0:rings])


def generateHexArrayCoordinatesWithinMaxRadius(
    hexPitch: float, maxRadius: float, nFuelAssemMax: int = 5000
) -> Tuple[List, List, int, List]:
    """
    Generate (x,y) coordinates for hex assembly array locations/elements.

    Parameters
    ----------
    hexPitch : float
        The hexagonal pitch (equal to the distance between nearest hex array
        location/element neighbors).
    maxRadius : float
        The maximum core radius (from the core center) of the center of any
        hexagonal array location/element.
    nFuelAssemMax : int
        The maximum number of hexagonal array locations/elements permitted.

    Returns
    -------
    xAssem : list of float
        A list of hex array x coordinates, assuming the core center is (0, 0).
    yAssem : list of float
        A list of hex array y coordinates, assuming the core center is (0, 0).
    nFuelAssem : int
        The total number of hex array locations/elements.
    """
    nFuelAssem = 0
    xAssem = []
    yAssem = []
    assemIndices = []
    for i in range(nFuelAssemMax):
        ij = ALL_HEX_RING_POS[i]
        (xCenter, yCenter) = getXYFromRingPos(
            ij, hexPitch, latticeType=2
        )  # assembly center, latticeType = 3 for TACO
        r = math.sqrt(xCenter ** 2 + yCenter ** 2)
        if r <= maxRadius:
            nFuelAssem += 1
            xAssem.append(xCenter)
            yAssem.append(yCenter)
            assemIndices.append(i + 1)

    return xAssem, yAssem, nFuelAssem, assemIndices


def getXYFromRingPos(
    ij: Tuple[int, int],
    pitch: float,
    latticeType: int = 3,
    start: int = 1,
    sign: int = -1,
) -> Tuple[float, float]:
    """
    Return the Cartesian (x, y) coodinates corresponding to (ring, pos) indices
    for a single hex lattice position. Optional parmeters start and sign
    determine which (ring, pos) ordering choice to use; default values
    correspond to the convention used by CURSOR for the MMR.

    Parameters
    ----------
    ij : (int, int)
        The hex lattice position (ring, pos) indices, starting from 1 (not 0).
    pitch : float
        The hex lattice "flat-to-flat" pitch (cm).
    latticeType : int
        2 for X-type lattice in the TACO core
        3 for Y-type lattice in the MMR core (default)
    start : int
        The hex "corner" index from which the (ring, pos) indexing begins on a
        given ring. Each ring has 6 "corners" indexed 0-5 starting with the
        upper right corner (60 degrees above horizontal) and moving
        counterclockwise.
    sign : int
        Whether the (ring, pos) indexing moves clockwise or counter clockwise
        from the starting position on each ring (-1 for clockwise, 1 for
        counterclockwise).

    Returns
    -------
    (x, y) : (float, float)
        The Cartesian coordinates for this hex lattice position (cm).
    """
    if not (latticeType in [2, 3]):
        raise Exception("The Serpent lattice type must be either 2 or 3!")

    if latticeType == 2:
        start = start + 0.5

    (i, j) = ij

    if i == 1:  # central hex (ring 1)
        (x, y) = (0.0, 0.0)
    else:
        theta30 = math.pi / 6
        theta60 = math.pi / 3
        # number of hexes in each of 6 ring edges
        jRing = ITEMS_PER_HEX_RING[i - 1] / 6
        edgeIndex = math.floor((j - 1) / jRing)  # ring edge index (0 to 5)
        jOnEdge = math.fmod(
            j - 1, i - 1
        )  # position index within ring edge (0 to jRing-1)

        r = math.sqrt(
            (i - 1) ** 2 + jOnEdge ** 2 - 2 * (i - 1) * jOnEdge * math.cos(theta60)
        )  # Law of Cosines
        # Law of Sines
        thetaOnEdge = math.asin(math.sin(theta60) * jOnEdge / r)
        theta0 = theta30 + theta60 * start
        theta = theta0 + sign * (theta60 * edgeIndex + thetaOnEdge)

        x = pitch * r * math.cos(theta)
        y = pitch * r * math.sin(theta)

    return (x, y)


def circularGridGen(
    elementRings: int, elementPitch: float
) -> Tuple[np.ndarray, np.ndarray]:
    """
    Circular lattice from center to assemblyR with minimum pitch coolChanPitch.

    The outer wall is not considered in this calculation, the
    lattice should fit into a circular region

    Each point in the line is copied N times and then rotated about
    the origin to have equal angular spacing within the ring. N is
    the number of elements within the relevant ring (1, 6, 12, ...)

    Parameters
    ----------
    elementRings : int
        Number of circular rings of channels
    elementPitch : float
        Distance between neighboring lattice elements

    Returns
    -------
    numpy.array
        X locations
    numpy.array
        Y locations

    """
    xCoolChan = []
    yCoolChan = []

    # else the one calculated previously is correct
    # create the points that go from the center outwards all equally spaced
    # based on the min web.
    center_points = [[0, i * elementPitch, 0] for i in range(elementRings)]
    # loop over each ring then each channel in the ring, rotating the point to
    # equally space channels in the ring
    for i in range(elementRings):
        channels_in_this_ring = getNumberOfHexPositionsInFullRings(
            i + 1
        ) - getNumberOfHexPositionsInFullRings(i)
        for channel_counter in range(channels_in_this_ring):
            angle = 360 / channels_in_this_ring * channel_counter
            rotated_p = rotate_around_point_z_direction(center_points[i], angle)
            xCoolChan.append(rotated_p[0])
            yCoolChan.append(rotated_p[1])

    return np.array(xCoolChan), np.array(yCoolChan)


def rotate_around_point_z_direction(
    p: List,
    angle: float,
    around_p: list = None,
) -> List[float]:
    """
    Rotate a point about another point using the Z axis as the rotation axis

    Parameters
    ----------
    p : list
        len 2 or 3 of a point to rotate, ex: [3,4]
    angle : float
        radians, counter-clockwise
    around_p : list
        len of p of a point to rotate

    Returns
    -------
    list
        same shape as p, point rotated about around_p
    """
    if around_p is None:
        around_p = [0.0] * len(p)
    else:
        assert len(p) == len(
            around_p
        ), "Point to rotate and point to rotate around must be the same size tuple"

    # translate the point to be rotated to the origin by setting the origin to
    # be around_p
    if len(p) == 3:
        rotated_point = [p[0] - around_p[0], p[1] - around_p[1], p[2]]
    elif len(p) == 2:
        rotated_point = [p[0] - around_p[0], p[1] - around_p[1]]
    else:
        raise IndexError("point to rotate must be a 2 or 3-tuple")

    # rotate the point about the origin
    s = math.sin(math.radians(angle))
    c = math.cos(math.radians(angle))
    tmp_pt = list(rotated_point)

    # perform the rotation trig and translate the point back to the original
    # coordinates
    rotated_point[0] = tmp_pt[0] * c - tmp_pt[1] * s + around_p[0]
    rotated_point[1] = tmp_pt[0] * s + tmp_pt[1] * c + around_p[1]

    return rotated_point


def removeFuelAssembliesOverlappingControlDrum(
    xyDrums: Tuple[float, float],
    controlDrumRadius: float,
    gapThickness: float,
    minSpacingDrumsAssemblies: float,
    assemblyDiameter: float,
    xAssem: List[float],
    yAssem: List[float],
):
    """
    Remove fuel assemblies that overlap control drums.

    Parameters
    ----------
    geom : dict
        A dictionary containing nearly all input parameters.
    xyDrums : list of tuple of float
        A list containing (x, y) tuple coordinates for control drum centers.
    """

    controlDrumRadiusIncGap = controlDrumRadius + gapThickness

    vetted_xAssem = []
    vetted_yAssem = []

    unique_combinations = list(itertools.product(xyDrums, zip(xAssem, yAssem)))

    for index, (xyDrum, xyAssem) in enumerate(unique_combinations):
        (xDrum, yDrum) = xyDrum
        (xCoor, yCoor) = xyAssem

        # Check if the pair of coordinates already exists in vetted_xAssem
        #  and vetted_yAssem at the same index
        if (xCoor, yCoor) in zip(vetted_xAssem, vetted_yAssem):
            continue

        coreDistWithHalfAssemDiam = math.sqrt(xCoor ** 2 + yCoor ** 2)
        coreDistWithHalfAssemDiam += assemblyDiameter / 2

        drumCenterDistanceToCoreCenter = math.sqrt(xDrum ** 2 + yDrum ** 2)
        drumCoreDistMinusGap = drumCenterDistanceToCoreCenter - controlDrumRadiusIncGap

        # check if assemblies are placed further than control drums
        # check if the minspacing requirements are met between drums
        #  and assemblies
        if coreDistWithHalfAssemDiam >= drumCoreDistMinusGap:
            # print(f"Drums {drumCoreDistMinusGap} and Assembly {coreDistWithHalfAssemDiam}")
            continue
        elif (
            drumCoreDistMinusGap - coreDistWithHalfAssemDiam
        ) <= minSpacingDrumsAssemblies:
            spacing = drumCoreDistMinusGap - coreDistWithHalfAssemDiam
            print(f"Drums {drumCoreDistMinusGap} and MinSpacing {spacing}")
            continue
        else:
            vetted_xAssem.append(xCoor)
            vetted_yAssem.append(yCoor)

    return vetted_xAssem, vetted_yAssem


def getCircularPackingCoordinates(
    assemblyPitch: float,
    coreRefOD: float,
    xyDrums: float,
    controlDrumRadius: float,
    gapThickness: float,
    minDrumAssemblySpacing: float,
    assemblyDiameter: float,
    assemblyGap: float = 0.0,
) -> Tuple[np.ndarray, np.ndarray]:
    """
    Generates circular coordinates and removes overlapping assemblies.

    Parameters:
    -----------
    assemblyPitch : float
        Pitch of the assemblies (assemblyDiameter + assemblyWebThickness).
    coreRefOD : float
        Reference outer diameter of the core where the drums end.
    xyDrums : float
        XY positions of the drums.
    controlDrumRadius : float
        Radius of the control drum.
    gapThickness : float
        Spacing between drums.
    minDrumAssemblySpacing : float
        Minimum spacing between assemblies.
    assemblyDiameter : float
        Diameter of the assembly.
    assemblyGap : float, optional
        Gap between assemblies. Defaults to 0.0.

    Returns:
    --------
    Tuple[np.ndarray, np.ndarray]
        Tuple containing the x and y coordinates of the generated assemblies.

    """
    _, nActiveAssemblyRings = getAssemblyHexArraySize(
        assemblyPitch, assemblyGap, coreRefOD
    )
    xAssem, yAssem = circularGridGen(nActiveAssemblyRings, assemblyPitch)
    xAssem, yAssem = removeFuelAssembliesOverlappingControlDrum(
        xyDrums,
        controlDrumRadius,
        gapThickness,
        minDrumAssemblySpacing,
        assemblyDiameter,
        xAssem,
        yAssem,
    )

    return xAssem, yAssem


def getHexPackingCoordinates(
    assemblyPitch: float,
    coreRefOD: float,
    xyDrums: float,
    controlDrumRadius: float,
    gapThickness: float,
    minDrumAssemblySpacing: float,
    assemblyDiameter: float,
    assemblyGap: float = 0.0,
) -> Tuple[np.ndarray, np.ndarray]:
    """
    Generates Hex Coordinates and removes assemblies overlapping with drums.

    Parameters
    ----------
    assemblyPitch : float
        Pitch of the assemblies.
    coreRefOD : float
        Reference outer diameter of the core where the drums end.
    xyDrums : float
        XY positions of the drums.
    controlDrumRadius : float
        Radius of the control drum.
    gapThickness : float
        Spacing between drums.
    minDrumAssemblySpacing : float
        Minimum spacing between assemblies.
    assemblyDiameter : float
        Diameter of the assembly.
    assemblyGap : float, optional
        Gap between assemblies. Defaults to 0.0.

    Returns
    -------
    Tuple[np.ndarray, np.ndarray]
        Tuple containing the x and y coordinates of the generated assemblies.

    """
    xAssem, yAssem, _, _ = generateHexArrayCoordinatesWithinMaxRadius(
        assemblyPitch, coreRefOD, nFuelAssemMax=5000
    )
    xAssem, yAssem = removeFuelAssembliesOverlappingControlDrum(
        xyDrums,
        controlDrumRadius,
        gapThickness,
        minDrumAssemblySpacing,
        assemblyDiameter,
        xAssem,
        yAssem,
    )

    return xAssem, yAssem
