"""Script that can be used to generate drum location and YAML blueprint blocks"""
import math
import string
from itertools import islice
from typing import Iterable
from functools import wraps

import numpy

# USER MODIFICATIONS
N_DRUMS = 14
RPV_ID = 118.73
"""RPV inner diameter [cm]"""
DRUM_OD = 13.0
"""Drum outer diameter [cm]"""
DRUM_RPV_GAP = 1.05
"""Gap between RPV and edge of drum [cm]"""
DRUM_ANGLE_SECTOR_WIDTH = 115.0
"""Angle width of the control drum sector [degrees]"""
DRUM_ANGLE_ROTATION = 180.0
"""Rotation away from drums in [degrees] (180 is drums out)"""
START_DRUM_XSTYPE = "D"
"""xs type for the first drum"""
START_DRUM_XSTYPE = "D"
"""xs type for the first drum"""
DRUM_ANCHOR_NAME = "block_drum"
"""block anchor name (it can be left as is here)"""
CONTROL_DRUM_BLOCK_TEMPLATE = string.Template(
    f"""  drum$i: &{DRUM_ANCHOR_NAME}$i
        center: *comp_drum_center
        complement: *comp_drum_complement
        control plate:
            shape: RadialSector
            material: B4C
            Tinput: 900.0
            Thot: 900.0
            id: 12.85
            od: 13
            flags: POISON CONTROL
            start_angle: $start
            end_angle: $end"""
)
"""Template control drum block.
   User can modify everything except variables
   wildcarded with the $ sign (e.g. start_angle, etc.)
"""
CONTROL_DRUM_ASSEMBLY_TEMPLATE = string.Template(
    f"""  drum$i:
        specifier: CD$i
        blocks:
        - *{DRUM_ANCHOR_NAME}$i
        flags: CONTROL
        height: *heights
        axial mesh points: *mesh
        xs types: [$xstype]"""
)
"""Template control drum assembly.
   User can modify everything except variables
   wildcarded with the $ sign (e.g. start_angle, etc.)
"""

# END MODIFICATIONS

# Checks
assert 0 < DRUM_ANGLE_SECTOR_WIDTH < 180
assert 0 <= DRUM_ANGLE_ROTATION <= 180
# end checks


def separated(f):
    @wraps(f)
    def inner(*args, **kwargs):
        print("-" * 80)
        return f(*args, **kwargs)

    return inner


@separated
def print_drum_coords(xc: Iterable[float], yc: Iterable[float]):
    for i, (x, y) in enumerate(zip(xc, yc)):
        print(f"CD{i} {round(x, 8):.8g} {round(y, 8):.8g}")


def bound_angle(a: float) -> float:
    if a < 0:
        return math.tau + a
    if a >= math.tau:
        return a - math.tau
    return a


@separated
def print_simple_drum_angles(
    start_angles: Iterable[float], end_angles: Iterable[float]
):
    for i, (start_angle, end_angle) in enumerate(zip(start_angles, end_angles)):
        start = round(math.degrees(start_angle), 8)
        end = round(math.degrees(end_angle), 8)
        print(f"CD{i} start: {start:.8g}, end: {end:.8g}")


@separated
def print_drum_block_templates(
    start_angles: Iterable[float], end_angles: Iterable[float]
):
    for i, (start_angle, end_angle) in enumerate(zip(start_angles, end_angles)):
        start = round(math.degrees(start_angle), 8)
        end = round(math.degrees(end_angle), 8)
        print(
            CONTROL_DRUM_BLOCK_TEMPLATE.substitute(
                i=str(i), start=f"{start:.8g}", end=f"{end:.8g}"
            )
        )


@separated
def print_assembly_templates(c: str):
    xs_char_index = string.ascii_uppercase.index(c)
    xstypes = islice(
        string.ascii_uppercase,
        xs_char_index,
        xs_char_index + N_DRUMS,
    )
    for i, xstype in enumerate(xstypes):
        print(CONTROL_DRUM_ASSEMBLY_TEMPLATE.substitute(i=str(i), xstype=xstype))


if __name__ == "__main__":
    rpv_inner_rad = RPV_ID * 0.5
    drum_center_distance = rpv_inner_rad - DRUM_RPV_GAP - 0.5 * DRUM_OD
    angle_drum_sep = math.tau / N_DRUMS
    drum_indices = numpy.arange(N_DRUMS)
    drum_internal_angle = drum_indices * angle_drum_sep
    drum_x_coords = drum_center_distance * numpy.cos(drum_internal_angle)
    drum_y_coords = drum_center_distance * numpy.sin(drum_internal_angle)

    drum_width = math.radians(DRUM_ANGLE_SECTOR_WIDTH)
    drum_angle_offset = math.radians(DRUM_ANGLE_ROTATION)

    drum_start_angles = numpy.empty_like(drum_x_coords)
    drum_end_angles = numpy.empty_like(drum_start_angles)

    for i, mid_angle in enumerate(drum_internal_angle):
        start_angle = bound_angle(
            math.pi - mid_angle + drum_angle_offset - 0.5 * drum_width
        )
        end_angle = bound_angle(start_angle + drum_width)
        drum_start_angles[i] = start_angle
        drum_end_angles[i] = end_angle

    print_drum_coords(drum_x_coords, drum_y_coords)

    print_simple_drum_angles(drum_start_angles, drum_end_angles)

    print_drum_block_templates(drum_start_angles, drum_end_angles)

    print_assembly_templates(START_DRUM_XSTYPE)
