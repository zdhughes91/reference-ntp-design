import numpy as np
from reactor_layout_utils import getCircularPackingCoordinates
from reactor_layout_utils import circularGridGen
import matplotlib.pyplot as plt

#x = getCircularPackingCoordinates(
#    assemblyPitch = 0.2 + 0.025 + 0.3,
#    coreRefOD = 6.35,
#    xyDrums = 0,
"""     controlDrumRadius = 0.2 + 0.025,
    gapThickness = 0.3,
    minDrumAssemblySpacing = 0.2,
    assemblyDiameter = 0.2 + 0.025 + 0.3,
    assemblyGap = 0.3,
)  """

center = [0,0]

channelPerCircle = [6,12,18,24,30]

coolantChannelPitch = 0.1 + 0.025 + 0.3 + 0.1 + 0.025
# coolantChannelLocs = circularGridGen(6,0.3+0.225) # old 
coolantChannelLocs = circularGridGen(6,coolantChannelPitch) # new

#              assemblyDiameter + spacingBWassemblies
assemblyPitch = 7.4 + 2.5
assemblyLocs = circularGridGen(5,assemblyPitch)
print(coolantChannelPitch)

def print_readable(locs):   
    for i in range(np.shape(locs)[1]):
        x = locs[0][i]
        y = locs[1][i]
        print(f"FS {x:.8g} {y:.8g}")
def show_locs(locs):
    plt.scatter(locs[0],locs[1])
    plt.show()

print_readable(coolantChannelLocs)
# print_readable(assemblyLocs)
# show_locs(coolantChannelLocs)
# show_locs(assemblyLocs)