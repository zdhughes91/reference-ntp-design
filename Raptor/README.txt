# =================================================================================== #
# =================================================================================== #
# ======================= -- Summer 2023 Raptor Modeling -- ======================= #
# ======================      --  Ultra Safe Nuclear  --     ======================== #
# =================================================================================== #
# ============= Zach Hughes - zhughes@tamu.edu - z.hughes@usnc-tech.com ============= #
# Folders:                                                                            #
# 1. All pretty self explanatory.                                                     #
# - Should note that there are several blueprints in the blueprint folder. The most   #
#   up-to-date one is *blueprint2revised.yaml, although this still needs improvements #
#   These improvements include:                                                       #
#   1. Beryllium (and all materials) need impurities                                  #
#   2. Double check all temperatures are correct                                      #
# Files:                                                                              #
# 1. Scripts/refDesignLatMap.py: This is where I got the locations for all fuel       #
#    assemblies + coolant channels for use in the blueprint.                          #
# ----------------------------------------------------------------------------------- #
# -- To Do:                                                                           #
#                                                                                     #
# ----------------------------------------------------------------------------------- #
# Sources:                                                                            #
# -- Hydrogen Properties                                                              #
# 1. https://webbook.nist.gov/cgi/fluid.cgi?P=3.7&TLow=80&THigh=1000&TInc=10&Digits=5&ID=C1333740&Action=Load&Type=IsoBar&TUnit=K&PUnit=MPa&DUnit=kg%2Fm3&HUnit=kJ%2Fkg&WUnit=m%2Fs&VisUnit=Pa*s&STUnit=N%2Fm&RefState=DEF
# ^ CoolProp Python API was also used to access NIST database for liqH ^              #
# -- UZrC Properties                                                                  #
# 2. https://www.sciencedirect.com/science/article/pii/S2352179122001715#s0040        #
# 3. https://www.osti.gov/biblio/4419566                                              #
# 4. ZrC kappa : https://www.osti.gov/biblio/4003326                                  #
# 5. ZrC Cp: https://www.sciencedirect.com/science/article/abs/pii/S0022311513007824  #
# =================================================================================== #
