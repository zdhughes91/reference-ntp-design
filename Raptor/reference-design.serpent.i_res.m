
% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1:  13]) = 'Serpent 2.2.0' ;
COMPILE_DATE              (idx, [1:  20]) = 'May 25 2023 09:09:06' ;
REVISION                  (idx, [1: 40])  = 'd4c590f74246425fc34969ee41095d558c954e46' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1:   8]) = 'Untitled' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:  26]) = 'reference-design.serpent.i' ;
WORKING_DIRECTORY         (idx, [1:  58]) = '/home/zhughes/.armi/0-20230713102505450917/temp-4wjqzw8SqA' ;
HOSTNAME                  (idx, [1:  15]) = 'DESKTOP-2H5A1KL' ;
CPU_TYPE                  (idx, [1:  35]) = '13th Gen Intel(R) Core(TM) i7-1370P' ;
CPU_MHZ                   (idx, 1)        = 4294967295.0 ;
START_DATE                (idx, [1:  24]) = 'Thu Jul 13 10:25:06 2023' ;
COMPLETE_DATE             (idx, [1:  24]) = 'Thu Jul 13 10:33:40 2023' ;

% Run parameters:

POP                       (idx, 1)        = 10000 ;
CYCLES                    (idx, 1)        = 100 ;
SKIP                      (idx, 1)        = 150 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 123456789 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000 ;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 0 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 0 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ] ;
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;

CRIT_SPEC_MODE            (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 1 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 4 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 1 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 1 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 8 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   8]) = [  1.00187E+00  1.00314E+00  9.97901E-01  1.00203E+00  9.91439E-01  1.00461E+00  1.00002E+00  9.98985E-01  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;
OMP_SHARED_QUEUE_LIM      (idx, 1)        = 0 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1:  51]) = '/home/zhughes/projects/xs-fetch/ENDFB8/xsdir_endfb8' ;
DECAY_DATA_FILE_PATH      (idx, [1:   3]) = 'N/A' ;
SFY_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
NFY_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
BRA_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 0.0E+00  0.00000E+00 0.0E+00 ];
DT_THRESH                 (idx, [1:   2]) = [  9.00000E-01  9.00000E-01 ] ;
ST_FRAC                   (idx, [1:   4]) = [  4.51919E-01 0.00053  0.00000E+00 0.0E+00 ];
DT_FRAC                   (idx, [1:   4]) = [  5.48081E-01 0.00043  0.00000E+00 0.0E+00 ];
DT_EFF                    (idx, [1:   4]) = [  4.00627E-01 0.00022  0.00000E+00 0.0E+00 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  5.54792E-01 0.00025  0.00000E+00 0.0E+00 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  5.38151E+00 0.00090  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  1.12970E+02 0.00070  0.00000E+00 0.0E+00 ];
AVG_REAL_COL              (idx, [1:   4]) = [  1.12809E+02 0.00070  0.00000E+00 0.0E+00 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  9.05260E+01 0.00058  0.00000E+00 0.0E+00 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  7.18081E+01 0.00116  0.00000E+00 0.0E+00 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 100 ;
SIMULATED_HISTORIES       (idx, 1)        = 999872 ;
MEAN_POP_SIZE             (idx, [1:   2]) = [  9.99872E+03 0.00186 ] ;
MEAN_POP_WGT              (idx, [1:   2]) = [  9.99872E+03 0.00186 ] ;
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  6.57011E+01 ;
RUNNING_TIME              (idx, 1)        =  8.57963E+00 ;
INIT_TIME                 (idx, [1:   2]) = [  4.56450E-01  4.56450E-01 ] ;
PROCESS_TIME              (idx, [1:   2]) = [  6.73333E-03  6.73333E-03 ] ;
TRANSPORT_CYCLE_TIME      (idx, [1:   3]) = [  8.11637E+00  8.11637E+00  0.00000E+00 ] ;
MPI_OVERHEAD_TIME         (idx, [1:   2]) = [  0.00000E+00  0.00000E+00 ] ;
ESTIMATED_RUNNING_TIME    (idx, [1:   2]) = [  8.57953E+00  0.00000E+00 ] ;
CPU_USAGE                 (idx, 1)        = 7.65780 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  7.99987E+00 0.00018 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.43954E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 11967.29 ;
ALLOC_MEMSIZE             (idx, 1)        = 3806.33 ;
MEMSIZE                   (idx, 1)        = 3663.54 ;
XS_MEMSIZE                (idx, 1)        = 3402.12 ;
MAT_MEMSIZE               (idx, 1)        = 189.99 ;
RES_MEMSIZE               (idx, 1)        = 0.49 ;
IFC_MEMSIZE               (idx, 1)        = 0.00 ;
MISC_MEMSIZE              (idx, 1)        = 70.94 ;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00 ;
UNUSED_MEMSIZE            (idx, 1)        = 142.79 ;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 465 ;
UNION_CELLS               (idx, 1)        = 32 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  0.00000E+00 ;
NEUTRON_ERG_NE            (idx, 1)        = 802645 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 30 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 59 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 59 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 0 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 0 ;
TOT_REA_CHANNELS          (idx, 1)        = 1629 ;
TOT_TRANSMU_REA           (idx, 1)        = 0 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 1 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Energy deposition:

EDEP_MODE                 (idx, 1)        = 0 ;
EDEP_DELAYED              (idx, 1)        = 1 ;
EDEP_KEFF_CORR            (idx, 1)        = 1 ;
EDEP_LOCAL_EGD            (idx, 1)        = 0 ;
EDEP_COMP                 (idx, [1:   9]) = [ 0 0 0 0 0 0 0 0 0 ] ;
EDEP_CAPT_E               (idx, 1)        =  0.00000E+00 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  0.00000E+00 ;
TOT_DECAY_HEAT            (idx, 1)        =  0.00000E+00 ;
TOT_SF_RATE               (idx, 1)        =  0.00000E+00 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  0.00000E+00 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  0.00000E+00 ;
INHALATION_TOXICITY       (idx, 1)        =  0.00000E+00 ;
INGESTION_TOXICITY        (idx, 1)        =  0.00000E+00 ;
ACTINIDE_INH_TOX          (idx, 1)        =  0.00000E+00 ;
ACTINIDE_ING_TOX          (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  0.00000E+00 ;
SR90_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
TE132_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
I131_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
I132_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
CS134_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
CS137_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
PHOTON_DECAY_SOURCE       (idx, [1:   2]) = [  0.00000E+00  0.00000E+00 ] ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  0.00000E+00 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  0.00000E+00 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  0.00000E+00 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  1.57583E+15 0.00159  0.00000E+00 0.0E+00 ];

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  1.96533E-01 0.00342 ];
U235_FISS                 (idx, [1:   4]) = [  6.93756E+18 0.00155  9.97689E-01 6.8E-05 ];
U238_FISS                 (idx, [1:   4]) = [  1.52974E+16 0.03062  2.20069E-03 0.03070 ];
U235_CAPT                 (idx, [1:   4]) = [  1.55159E+18 0.00357  2.13431E-01 0.00330 ];
U238_CAPT                 (idx, [1:   4]) = [  1.63294E+18 0.00352  2.24602E-01 0.00295 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC      (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_SRC_NEUTRON_FISS     (idx, [1:   2]) = [ 999872 1.00000E+06 ] ;
BALA_SRC_NEUTRON_NXN      (idx, [1:   2]) = [ 0 7.67383E+04 ] ;
BALA_SRC_NEUTRON_VR       (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_SRC_NEUTRON_TOT      (idx, [1:   2]) = [ 999872 1.07674E+06 ] ;

BALA_LOSS_NEUTRON_CAPT    (idx, [1:   2]) = [ 429675 4.61380E+05 ] ;
BALA_LOSS_NEUTRON_FISS    (idx, [1:   2]) = [ 409755 4.41309E+05 ] ;
BALA_LOSS_NEUTRON_LEAK    (idx, [1:   2]) = [ 160442 1.74049E+05 ] ;
BALA_LOSS_NEUTRON_CUT     (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_NEUTRON_ERR     (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_NEUTRON_TOT     (idx, [1:   2]) = [ 999872 1.07674E+06 ] ;

BALA_NEUTRON_DIFF         (idx, [1:   2]) = [ 0 -2.44472E-08 ] ;

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   2]) = [  2.25000E+08 0.0E+00 ];
TOT_POWDENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_GENRATE               (idx, [1:   2]) = [  1.68744E+19 1.3E-06 ];
TOT_FISSRATE              (idx, [1:   2]) = [  6.94257E+18 9.8E-08 ];
TOT_CAPTRATE              (idx, [1:   2]) = [  7.26809E+18 0.00143 ];
TOT_ABSRATE               (idx, [1:   2]) = [  1.42107E+19 0.00073 ];
TOT_SRCRATE               (idx, [1:   2]) = [  1.57583E+19 0.00159 ];
TOT_FLUX                  (idx, [1:   2]) = [  3.30546E+21 0.00117 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  2.74310E+18 0.00330 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  1.69538E+19 0.00096 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  1.91080E+21 0.00121 ];
INI_FMASS                 (idx, 1)        =  0.00000E+00 ;
TOT_FMASS                 (idx, 1)        =  0.00000E+00 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  1.90183E+00 0.00075 ];
SIX_FF_F                  (idx, [1:   2]) = [  7.80524E-01 0.00088 ];
SIX_FF_P                  (idx, [1:   2]) = [  7.60853E-01 0.00082 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  1.14992E+00 0.00061 ];
SIX_FF_LF                 (idx, [1:   2]) = [  9.02157E-01 0.00038 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.15530E-01 0.00036 ];
SIX_FF_KINF               (idx, [1:   2]) = [  1.29870E+00 0.00126 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  1.07266E+00 0.00132 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.43058E+00 1.3E-06 ];
FISSE                     (idx, [1:   2]) = [  2.02280E+02 9.8E-08 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  1.07288E+00 0.00137  1.06553E+00 0.00133  7.12938E-03 0.01599 ];
IMP_KEFF                  (idx, [1:   2]) = [  1.07158E+00 0.00096 ];
COL_KEFF                  (idx, [1:   2]) = [  1.07110E+00 0.00159 ];
ABS_KEFF                  (idx, [1:   2]) = [  1.07158E+00 0.00096 ];
ABS_KINF                  (idx, [1:   2]) = [  1.29755E+00 0.00076 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.84419E+01 0.00023 ];
IMP_ALF                   (idx, [1:   2]) = [  1.84466E+01 8.2E-05 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  1.95975E-07 0.00429 ];
IMP_EALF                  (idx, [1:   2]) = [  1.94908E-07 0.00150 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  1.12800E-02 0.02697 ];
IMP_AFGE                  (idx, [1:   2]) = [  1.10017E-02 0.00210 ];

% Forward-weighted delayed neutron parameters:

PRECURSOR_GROUPS          (idx, 1)        = 6 ;
FWD_ANA_BETA_ZERO         (idx, [1:  14]) = [  6.93550E-03 0.01346  2.28989E-04 0.06331  1.24668E-03 0.03116  1.19324E-03 0.02939  2.73160E-03 0.02225  1.08556E-03 0.03393  4.49439E-04 0.04967 ];
FWD_ANA_LAMBDA            (idx, [1:  14]) = [  4.64197E-01 0.01969  1.26705E-02 0.02306  3.27373E-02 3.7E-05  1.20788E-01 3.5E-05  3.02883E-01 8.3E-05  8.49885E-01 0.00016  2.76900E+00 0.01768 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  14]) = [  6.49772E-03 0.01859  2.29089E-04 0.09280  1.14381E-03 0.05556  1.13925E-03 0.04433  2.49750E-03 0.03027  1.05889E-03 0.05104  4.29179E-04 0.08057 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  14]) = [  4.66710E-01 0.02941  1.33360E-02 6.9E-07  3.27387E-02 8.5E-06  1.20785E-01 3.2E-05  3.02837E-01 8.2E-05  8.50009E-01 0.00039  2.85464E+00 0.00029 ];

% Adjoint weighted time constants using Nauchi's method:

IFP_CHAIN_LENGTH          (idx, 1)        = 15 ;
ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  2.01299E-04 0.00278  2.01298E-04 0.00280  2.02866E-04 0.02405 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  2.15924E-04 0.00227  2.15922E-04 0.00229  2.17704E-04 0.02423 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  14]) = [  6.64957E-03 0.01690  2.24908E-04 0.08652  1.12899E-03 0.05043  1.16773E-03 0.04462  2.65073E-03 0.02661  1.02097E-03 0.04571  4.56241E-04 0.07341 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  14]) = [  4.71973E-01 0.02897  1.33360E-02 0.0E+00  3.27372E-02 5.5E-05  1.20789E-01 5.6E-05  3.02880E-01 0.00013  8.50190E-01 0.00040  2.85397E+00 0.00024 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  1.97590E-04 0.00592  1.97546E-04 0.00595  2.17468E-04 0.08104 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  2.11948E-04 0.00572  2.11901E-04 0.00577  2.33006E-04 0.08074 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  14]) = [  7.29859E-03 0.05842  2.00282E-04 0.40253  1.32018E-03 0.13890  1.32670E-03 0.15711  2.73710E-03 0.10050  1.12839E-03 0.16075  5.85936E-04 0.22803 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  14]) = [  5.07728E-01 0.09065  1.33360E-02 0.0E+00  3.27390E-02 0.0E+00  1.20780E-01 3.8E-09  3.02909E-01 0.00043  8.49490E-01 0.0E+00  2.85300E+00 5.4E-09 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  14]) = [  7.11481E-03 0.05577  2.14650E-04 0.40805  1.27980E-03 0.13565  1.28865E-03 0.14957  2.70561E-03 0.09351  1.10136E-03 0.15166  5.24730E-04 0.20288 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  14]) = [  5.07507E-01 0.08687  1.33360E-02 0.0E+00  3.27390E-02 0.0E+00  1.20780E-01 2.7E-09  3.02885E-01 0.00035  8.49490E-01 0.0E+00  2.85300E+00 5.4E-09 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [ -3.70926E+01 0.05911 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  1.99644E-04 0.00158 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  2.14158E-04 0.00093 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  6.63988E-03 0.01126 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -3.32642E+01 0.01127 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  9.42226E-07 0.00094 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  1.59719E-05 0.00051  1.59709E-05 0.00051  1.61269E-05 0.00581 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  2.61309E-04 0.00132  2.61314E-04 0.00132  2.60932E-04 0.01470 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  7.08491E-01 0.00081  7.08867E-01 0.00083  6.65626E-01 0.01890 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.08540E+01 0.02620 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  1.12809E+02 0.00070  1.31129E+02 0.00074 ];

