# =================================================================================== #
# =================================================================================== #
# ====================== Reference NTP Design Unit Cell Model ======================= #
# ======================      -- First Working Model --      ======================== #
# =================================================================================== #
# ========================= Zach Hughes - zhughes@tamu.edu  ========================= #
# Folders:                                                                            #
# 1. *FluidGasGap: This is the heterogenous case that treats the gas gap as a fluid   #
#    region. I have not gotten it to work yet.                                        #
# 2. *SolidGasGap: This treats the gas gap as a solid and is currently predicting a   #
#    max fuel temperature of ~3400 K.                                                 #
# ----------------------------------------------------------------------------------- #
# -- To Do:                                                                           #
#                                                                                     #
# ----------------------------------------------------------------------------------- #
# Sources:                                                                            #
# -- Hydrogen Properties                                                              #
# 1. https://webbook.nist.gov/cgi/fluid.cgi?P=3.7&TLow=80&THigh=1000&TInc=10&Digits=5&ID=C1333740&Action=Load&Type=IsoBar&TUnit=K&PUnit=MPa&DUnit=kg%2Fm3&HUnit=kJ%2Fkg&WUnit=m%2Fs&VisUnit=Pa*s&STUnit=N%2Fm&RefState=DEF
# ^ CoolProp Python API was also used to access NIST database for liqH ^              #
# -- UZrC Properties                                                                  #
# 2. https://www.sciencedirect.com/science/article/pii/S2352179122001715#s0040        #
# 3. https://www.osti.gov/biblio/4419566                                              #
# 4. ZrC kappa : https://www.osti.gov/biblio/4003326                                  #
# 5. ZrC Cp: https://www.sciencedirect.com/science/article/abs/pii/S0022311513007824  #
# =================================================================================== #
