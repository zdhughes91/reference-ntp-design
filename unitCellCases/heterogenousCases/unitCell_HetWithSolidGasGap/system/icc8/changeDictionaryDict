/*--------------------------------*- C++ -*----------------------------------*\
| =========                 |                                                 |
| \\      /  F ield         | OpenFOAM: The Open Source CFD Toolbox           |
|  \\    /   O peration     | Version:  v2212                                 |
|   \\  /    A nd           | Website:  www.openfoam.com                      |
|    \\/     M anipulation  |                                                 |
\*---------------------------------------------------------------------------*/
FoamFile
{
    version     2.0;
    format      ascii;
    class       dictionary;
    object      changeDictionaryDict;
}
// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

U
{
    internalField   uniform (0 0 -67.7);

    boundaryField
    {
    icc8
    {
        type            inletOutlet;
        inletValue      uniform (0 0 0);
        value           uniform (0 0 0);
    }
    icc8_top
    {
        type            fixedValue;
        value           uniform (0 0 -67.7);
    }
    icc8_to_icc8Clad
    {
        type            fixedValue;
        value           uniform (0 0 0);
    }
    ".*"
    {
        type            zeroGradient;
    }
    }
}

T
{
    internalField   uniform 300;

    boundaryField
    {
        icc8
        {
            type            inletOutlet;
            value           uniform 300;
            inletValue      uniform 300;
        }
        icc8_top
        {
            type            fixedValue;
            value           uniform 300;
        }
        icc8_to_icc8Clad
        {
            type            compressible::turbulentTemperatureRadCoupledMixed;
            Tnbr            T;
            kappaMethod     fluidThermo;
            value           uniform 300;
        }
        ".*"
        {
            type            zeroGradient;
        }
    }
}

epsilon
{
    internalField   uniform 37498.7028;

    boundaryField
    {
        icc8
        {
            type            zeroGradient;
        }
        icc8_top
        {
            type            turbulentMixingLengthDissipationRateInlet;
            mixingLength    0.00014; // calculated in mrf2DheatConduction.py
            value           $internalField;
        }
        icc8_to_icc8Clad
        {
            type            epsilonWallFunction;
            value           $internalField;
        }
        ".*"
        {
            type            zeroGradient;
        }
    }
}

k
{
    internalField   uniform 10.07;

    boundaryField
    {
        icc8
        {
            type            zeroGradient;
        }
        icc8_top
        {
            type            turbulentIntensityKineticEnergyInlet;
            intensity       0.040705; // *100, /100 do not change anything
            value           $internalField;
        }
        icc8_to_icc8Clad
        {
            type            kqRWallFunction;
            value           $internalField;
        }
        ".*"
        {
            type            zeroGradient;
        }
    }
}

p_rgh
{
    internalField   uniform 7.2e6;

    boundaryField
    {
        icc8
        {
            type            fixedValue;
            value           uniform 7e6;
        }

        icc8_top
        {
            type            fixedFluxPressure;
            value           $internalField;
        }

        icc8_to_icc8Clad
        {
            type            fixedFluxPressure;
            value           $internalField;
        }
        ".*" // this may be more stable as fixedfluxpressure...
        {
            type            zeroGradient;
        }
    }
}

p
{
    internalField   uniform 7.2e6;

    boundaryField
    {
        icc8
        {
            type            calculated;
            value           $internalField;
        }

        icc8_top
        {
            type            calculated;
            value           $internalField;
        }

        icc8_to_icc8Clad
        {
            type            calculated;
            value           $internalField;
        }
        ".*"
        {
            type            calculated;
            value           $internalField;
        }
    }
}

nut
{
    internalField   uniform 0;

    boundaryField
    {
        icc8
        {
            type            calculated;
            value           uniform 0;
        }
        icc8_top
        {
            type            calculated;
            value           uniform 0;
        }
        icc8_to_icc8Clad
        {
            type            nutkWallFunction;
            value           uniform 0;
        }
        ".*"
        {
            type            calculated;
            value           uniform 0;
        }
    }
}

alphat
{
    internalField   uniform 0;

    boundaryField
    {
        icc8
        {
            type            calculated;
            value           $internalField;
        }
        icc8_top
        {
            type            calculated;
            value           $internalField;
        }
        icc8_to_icc8Clad
        {
            type            compressible::alphatWallFunction;
            value           $internalField;
        }
        ".*"
        {
            type            calculated;
            value           uniform 0;
        }
    }
}
// ************************************************************************* //
