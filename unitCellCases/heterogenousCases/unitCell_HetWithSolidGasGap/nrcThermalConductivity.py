# Copyright 2023 USNC
# All rights reserved

from typing import List
from numpy import identity, zeros
from numpy.linalg import solve


def equivalentThermalConductivityOfCompact(
    packingFraction: float, layerRadii: List[float], layerConductivities: List[float]
) -> float:
    """Homogenized thermal conductivity of particle fuel compact

    Solves system of heat equations formed when modeling a sphere
    surrounded by concentric spheres (layers) of material all
    within another continuous matrix of material. Perfect thermal
    contact is assumed for all layers and a far field temperature
    boundary condition on the gradient is imposed by assuming
    spheres do not interact with one-another. This method was tested
    in [1] to agree well with finite element versions of the same cases.

    Parameters
    ----------
    packingFraction : float
        Volume fraction occupied by the particles in the matrix. Must be
        [0, 1] inclusive e.g., 60% packing fraction would be ``0.6``
    radii : list of float
        ``N`` sized list of particle fuel radii (cm) from smallest to largest.
    conductivities : list of float
        ``N + 1`` sized list of thermal conductivities. The first ``N`` items
        correspond to the particle fuel materials, from inner to outer layer.
        The last item is the thermal conductivity of the matrix material

    Returns
    -------
    float
        The effective conductivity with units equal to input conductivity units

    Notes
    -----
    The NRC reference has some typos. The matrix shown should
    have k_i multiplier in the A matrix's bottom right quadrant and
    the sign of that quadrant should be flipped. See the equation
    a few lines up from the matrix that act as the generator for
    the matrix

    References
    ----------
    [1] https://www.nrc.gov/docs/ML0909/ML090900017.pdf section C1.4 through C1.6

    """
    numCoatings = len(layerRadii) - 1
    squareMatrixSize = (numCoatings + 2) * 2
    A = identity(squareMatrixSize)

    # top right quadrant of matrix
    for row in range(numCoatings + 1):
        A[row, row + 1] = -1
    A[numCoatings, -2] = 1
    A[numCoatings, -1] = -1

    # top left quadrant of matrix
    for row, radius in zip(range(numCoatings), layerRadii[:-1]):
        col = row + numCoatings + 2
        A[row, col] = (layerRadii[-1] / radius) ** 3
        A[row, col + 1] = -A[row, col]

    # bottom left+right quadrants of matrix
    for row, kiMinus1, ki, radius in zip(
        range(numCoatings + 3, squareMatrixSize),
        layerConductivities[:-1],
        layerConductivities[1:],
        layerRadii,
    ):
        A[row, row - (numCoatings + 3)] = -kiMinus1
        A[row, row - (numCoatings + 2)] = ki

        col = row
        A[row, col] = -2 * (layerRadii[-1] / radius) ** 3 * ki
        A[row, col - 1] = 2 * (layerRadii[-1] / radius) ** 3 * kiMinus1
    b = zeros(squareMatrixSize)
    # entry nCoats + 2 has the far field BC applied so it's non-zero
    # -1 for python indexing
    b[numCoatings + 1] = 1.0

    x = solve(A, b)
    km = layerConductivities[-1]
    B_ncoatPlus2 = x[-1]
    kp = km * (1 - 2 * B_ncoatPlus2) / (1 + B_ncoatPlus2)

    tc = maxwellsAverageTC(kp, km, packingFraction)
    return tc,A


def maxwellsAverageTC(
    kParticle: float, kMatrix: float, packingFraction: float
) -> float:
    """Maxwell's method of combining thermal conductivities for a packed particle matrix

    Assumes kParticle and kMatrix are defined on the same temperatures.
    If kParticle > kMatrix then the maxwell method is used,
    if not the inverted maxwell is used

    Parameters
    ----------
    kParticle : float
        The thermal conductivity of the fuel particle
    kMatrix : float
        The thermal conductivity of the matrix material
    packingFraction : float
        The particle packing fraction (0<x<1)

    Returns
    -------
    float
        The effective conductivity

    References
    ----------
    [1] Folsom, C. P. (2012). Effective Thermal Conductivity of
    Tri-Isotropic (TRISO) Fuel Compacts. Utah State University. Master's thesis.

    """
    phi = packingFraction

    if packingFraction > 1 or packingFraction < 0:
        raise Exception(f"Packing fraction {packingFraction} must be between 0 and 1")

    # eq 2.6
    kappa = kParticle / kMatrix
    # eq 2.7
    beta = (kappa - 1) / (kappa + 2)

    # from [1]: The regular Maxwell’s equation is used when
    # the thermal conductivity of the dispersed
    # phase is less than the conductivity of the continuous phase.
    # Else use inverted Maxwell’s equation
    if kappa < 1:
        k_combined = (1 + 2 * beta * phi) / (1 - beta * phi) * kMatrix
    else:
        k_combined = (
            ((1 + 2 * beta * phi) * (1 - beta + 2 * beta * phi))
            / ((1 - beta) * (1 + 2 * beta - beta * phi))
            * kMatrix
        )

    return k_combined