import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from CoolProp.CoolProp import PropsSI
from scipy.optimize import curve_fit
# ================================================================================================================
#  -  # below is a list of sources for the thermophysical properties data --------------------------------------
#  1  # ZrC-Hf:k   = W/(m*K)   https://www.osti.gov/biblio/4003326
#  2  # ZrC-Hf:Cp  =  J/kg*K), https://www.sciencedirect.com/science/article/abs/pii/S0022311513007824
#  3  # ZrC-Hf:rho =  kg/m^3 , from baseline.mater
#  4  # UZrC:k     =  W/(m*K)  at 1500 K https://www.sciencedirect.com/science/article/pii/S2352179122001715#s0040 --> paragraph below Fig.12
#  5  # UZrC:Cp    =  J/(kg*K) at 2500 K https://www.sciencedirect.com/science/article/pii/S2352179122001715#s0040 --> Fig.9
#  6  # UZrC:rho   =  based on Performance of (U,Zr)C-graphite (composite) and of (U,Zr)C (carbide) fuel elements in the Nuclear Furnace 1 test reactor
#  -  # above is a list of sources for the thermophysical properties data --------------------------------------
# ================================================================================================================
class geometry:
    def __init__(self):
        """
        This class is mainly just for bookkeeping for how I found the geometry
        """
        self.geometricalProperties = {'coolantChannel':{'or':0.001}, # all units in m
                                      'fuelAssembly'  :{'or':0.03175},
                                      'assemblyCan'   :{'or':0.032},
                                      'insulator'     :{'or':0.034},
                                      'gasGap'        :{'or':0.037},
                                      'moderator'     :{'or':0.0 },
                                      'assemblyWeb'   :0.099       # this is the distance between origin of one assembly to origin of the clostest next assembly, taken from baseline.i
                                      }
        self.geometricalProperties['latticeSpacing'] = self.geometricalProperties['assemblyWeb'] - 2*self.geometricalProperties['gasGap']['or'] # the spacing between OD of one assembly to OD of closest next assembly
    def findHexBorders(self):
        """ 
        The distance of the unit cell hexagonal borders should be half the distance of the lattice spacing from the gasGap outer diameter. 
        The first line should be a translation of the y-axis in the x-direction of the distance mentioned above, and the second line should be
        the first line rotated 60 degrees about the z-axis
        """
        self.geometricalProperties['hexSpacing'] = self.geometricalProperties['latticeSpacing'] / 2

class preProcess: 
    def __init__(self):
        thermal_conductivity_Be = pd.read_csv('materialData/k_Be.csv')
        thermal_conductivity_zrc74 = pd.read_csv('materialData/k_ZrC74PerRD.csv') # porous ZrC
        thermal_conductivity_zrc93 = pd.read_csv('materialData/k_ZrC93PerRD.csv') # dense ZrC

        self.materialData = {'Be':thermal_conductivity_Be,
                             'ZrC74':thermal_conductivity_zrc74,
                             'ZrC93':thermal_conductivity_zrc93}
        self.materialData['Be']['rho'] = 1850           # kg/(m^3)
        self.materialData['ZrC74']['rho'] = 6723 * 0.7  # kg/(m^3)
        self.materialData['ZrC93']['rho'] = 6723        # kg/(m^3)
        self.materialData['Be']['Cp'] = 1820            # J/(kg-K)
        #self.materialData['ZrC74']['Cp'] = 6723 * 0.7   # J/(kg-K) ... defined in self.CpZrC
        #self.materialData['ZrC93']['Cp'] = 6723         # J/(kg-K) ... defined in self.CpZrC
        # ZrC Cp = 51.027+3.685*(temperature/1000)-0.199*(temperature/1000)^2+0.028*(temperature/1000)^3-1.304/(temperature/1000)^2

    def initializeLiqH(self):
        density = PropsSI('Dmass', 'T', 2500, 'P', 8e6, 'H2')
        specificHeat = PropsSI('Cp0mass','T',2500,'P',8e6,'H2')
        conductivity = PropsSI('conductivity','T',2500,'P',8e6, 'H2')
        self.materialData['liqH'] = {'rho':density,
                                     'Cp':specificHeat,
                                     'k':conductivity}
    def printLiqHValues(self):
        print(self.materialData['liqH'])

    def showValues(self):
        fig, ax1 = plt.subplots()
        ax1.set_ylabel("Thermal Conductivity (W/(m K))")
        ax1.set_xlabel("Temperature (K)")
        plt.title('Thermal Conductivities')
        ax1.plot(self.materialData['Be']['T'], self.materialData['Be']['k'], label='Be')
        ax1.plot(self.materialData['ZrC74']['T'], self.materialData['ZrC74']['k'], label='ZrC74')
        ax1.plot(self.materialData['ZrC93']['T'], self.materialData['ZrC93']['k'], label='ZrC93')
        ax1.xaxis.grid()
        ax1.yaxis.grid()
        ax1.set_xlim(300,1400)
        ax1.set_ylim(0,250)
        fig.set_dpi(100)
        fig.legend()
        plt.show()

    def find300point(self,yval):
        """This function finds where the temperature starts at 300 so I only curve fit in range that matters
        -- yval is a string of the material property that should be used as the y value in fitVectors"""
        d = self.materialData
        BeLoc = np.where(np.abs(d['Be']['T']-300) == np.min(np.abs(d['Be']['T']-300)))[0][0]
        Zr74loc = np.where(np.abs(d['ZrC74']['T']-300) == np.min(np.abs(d['ZrC74']['T']-300)))[0][0]
        Zr93loc = np.where(np.abs(d['ZrC93']['T']-300) == np.min(np.abs(d['ZrC93']['T']-300)))[0][0]
        # .values.flatten() converts from pandas dataframe to 1d nd arrays so scipy.curve_fit 
        Be_fitVectors = [d['Be']['T'][BeLoc:].values.flatten() , d['Be'][yval][BeLoc:].values.flatten() ]
        Zr74_fitVectors = [d['ZrC74']['T'][Zr74loc:].values.flatten() , d['ZrC74'][yval][Zr74loc:].values.flatten() ]
        Zr93_fitVectors = [d['ZrC93']['T'][Zr93loc:].values.flatten() , d['ZrC93'][yval][Zr93loc:].values.flatten() ]
        fitVectors = {'Be':Be_fitVectors,
                      'ZrC74':Zr74_fitVectors,
                      'ZrC93':Zr93_fitVectors}
        return fitVectors
    
    def polynomial8cf(self,t,c0,c1,c2,c3,c4,c5,c6,c7): # 
        """This function is used as the model function for the curve fitting command"""
        f = c0 + c1*t + c2*t**2 + c3*t**3 + c4*t**4 + c5*t**5 + c6*t**6 + c7*t**7
        return f
    
    def linearFit(self,t,c0,c1):
        """ linear fit option because polynomial goes below zero after 1500K"""
        f = c0 + c1*t
        return f
    
    def fitConductivity(self):
        """use scipy.curve_fit to find 7th order polynomial fit to data"""
        fitVectors = self.find300point('k')
        BeCoefs, pcov_Be = curve_fit(self.polynomial8cf,fitVectors['Be'][0],fitVectors['Be'][1])
        ZrC74Coefs, pcov_ZrC74 = curve_fit(self.linearFit,fitVectors['ZrC74'][0],fitVectors['ZrC74'][1])
        ZrC93Coefs, pcov_ZrC93 = curve_fit(self.linearFit,fitVectors['ZrC93'][0],fitVectors['ZrC93'][1])
        # below is printing out coefs for ZrC equaitons of state
        #print('74',ZrC74Coefs)
        #print('93',ZrC93Coefs)
        self.kappaFitCoefs = {'Be':BeCoefs,
                      'ZrC74':ZrC74Coefs,
                      'ZrC93':ZrC93Coefs}
        
    def plotKappaFit(self,plot=False):
        """plotting fits to visually confirm they describe the data"""
        self.fitConductivity()
        fig, ax1 = plt.subplots()
        ax1.set_ylabel("Thermal Conductivity (W/(m K))")
        ax1.set_xlabel("Temperature (K)")
        plt.title('Thermal Conductivities')
        ax1.plot(self.materialData['Be']['T'], self.materialData['Be']['k'], label='Be',c='r')
        ax1.plot(self.materialData['ZrC74']['T'], self.materialData['ZrC74']['k'], label='ZrC74',c='b')
        ax1.plot(self.materialData['ZrC93']['T'], self.materialData['ZrC93']['k'], label='ZrC93',c='g')
        ax1.plot(np.linspace(0,2700,200), self.polynomial8cf(np.linspace(0,2700,200),*self.kappaFitCoefs['Be']), label='Be Fit',c='r',linestyle='dashed')
        ax1.plot(np.linspace(0,2700,200), self.linearFit(np.linspace(0,2700,200),*self.kappaFitCoefs['ZrC74']), label='ZrC74 Fit',c='b',linestyle='dashed')
        ax1.plot(np.linspace(0,2700,200), self.linearFit(np.linspace(0,2700,200),*self.kappaFitCoefs['ZrC93']), label='ZrC93 Fit',c='g',linestyle='dashed')
        ax1.xaxis.grid()
        ax1.yaxis.grid()
        ax1.set_xlim(300,2500)
        ax1.set_ylim(0,250)
        fig.set_dpi(100)
        fig.legend()
        if plot: plt.show()

    def printCoefs(self,cname,name,property): # c is coefs
        """print coefficients in a way that they can easily be copy/paste into openfoam dict"""
        if property == "k":
            c = self.kappaFitCoefs[cname]
        else:
            c = self.muFitCoefs[cname]
        print(' ==== ',cname,' ==== ')
        print(name+'<8> (')
        print('\t'+"{0:.7E}".format(c[0]),"{0:.7E}".format(c[1]),"{0:.7E}".format(c[2]),"{0:.7E}".format(c[3]))
        print('\t'+"{0:.7E}".format(c[4]),"{0:.7E}".format(c[5]),"{0:.7E}".format(c[6]),"{0:.7E}".format(c[7]))
        print(');')

    def printKappaCoefs(self):
        """call all mats from same place"""
        self.printCoefs('Be','kappaCoeffs','k')
        self.printCoefs('ZrC74','kappaCoeffs','k')
        self.printCoefs('ZrC93','kappaCoeffs','k')

    def CpZrC(self,showValue=False):
        """This function is made to find the equation of state of ZrC to be input into OpenFOAM
        Eqn: Cp 51.027+3.685*(temperature/1000)-0.199*(temperature/1000)^2+0.028*(temperature/1000)^3-1.304/(temperature/1000)^2
        - The above equation was given by Nolan, REFERENCE: Harrison, R. W., and W. E. Lee. "Processing and Properties of ZrC, ZrN and ZrCN Ceramics: A Review." Advances in Applied Ceramics 115, no. 5 (2016): 294-307. 
        -***- if you plot this in desmos it is clear that Cp for all reasonable values is 51 J/(mol-K). It is logarithmic"""
        #converting from J/(mol-K) to J/(kg-K)
        molarMass = 91.224 + 12.0107 
        self.materialData['ZrC74']['Cp'] = 51*(1/molarMass)*1000
        self.materialData['ZrC93']['Cp'] = 51*(1/molarMass)*1000
        if showValue: print('ZrC Cp = {:0.2f} J/(kg-K)'.format(self.materialData['ZrC74']['Cp'])) # 1000 g/kg

    def printZrCrho(self):
        rho = 6723
        print('rhoZrC    = '+str(rho)+' kg/m^3')
        print('rhoZrC0.7 = {:0.1f} kg/m^3'.format(rho*0.7))
    
    def sourceFuelEquation(self,t):
        """This is equation (6) from the paper listed at the top of this script in #4+5. "Synthesis, thermal conductivity..." """
        Cp = 0.43229 + (-3.2916*(10**-5)*t) + (-12712*t**-2) # eqn. 6 is given in J/(g-K)
        Cp = Cp*1000 # converting to J/(kg-K)
        return Cp
    
    def initializeFuel(self):
        "initializing fuel mateial properties by inputting the values into the materialData dictionary"
        self.materialData['UZrC']['T'] = np.linspace(300,3100,200)
        self.materialData['UZrC']['Cp'] = self.sourceFuelEquation(self.materialData['UZrC']['T'])
        self.materialData['UZrC']['rho'] = 5458 

class TRISO:
    def __init__(self):
        # geometrical + thermophysicalProperties comes from Table 2.4.1
        #all units in m
        self.geometricalProperties = {'U'   :{'or':500e-6}, #uranium
                                      'ipyc':{'or':690e-6}, #porous carbon
                                      'buff':{'or':770e-6}, #pyrolytic carbon
                                      'opyc':{'or':840e-6}, #silicon carbide
                                      'zrc' :{'or':920e-6}, #pyrolytic carbon
                                      'alph':0.09344} # packing fraction (pg7-2)
        
        #all units in W/(m K)
        self.thermophysicalProperties = {'U'   :{'k':3.7}, #uranium
                                        'ipyc':{'k':0.5},  #porous carbon
                                        'buff':{'k':4.0},  #pyrolytic carbon
                                        'opyc':{'k':16.0}, #silicon carbide
                                        'zrc' :{'k':4.0},  #pyrolytic carbon
                                        'matr' :{'k':15}}  #graphite as matrix material
        self.outerCoatingMat = 'zrc' # material of the outermost triso layer
        
    def buildUpperLeftQuadrant(self):
        """
        builds the upper-left quadrant of the matrix being built in the buildMatrix function
        """
        upperLeft = np.eye(self.Ncoat+2)
        for i in range(self.Ncoat+1): upperLeft[i,i+1] = -1
        return upperLeft
        
    def buildLowerLeftQuadrant(self):
        lowerLeft = np.zeros((self.Ncoat+2,self.Ncoat+2))
        for i, mat in enumerate(self.thermophysicalProperties):
            if i == 0: 
                lowerLeft[i+1,i] = -1 * self.thermophysicalProperties[mat]['k']
                continue
            if i == self.Ncoat+1:
                lowerLeft[i,i] = self.thermophysicalProperties[mat]['k']
                continue
            lowerLeft[i,i] = self.thermophysicalProperties[mat]['k']
            lowerLeft[i+1,i] = -1 * self.thermophysicalProperties[mat]['k']
        return lowerLeft
    
    def buildUpperRightQuadrant(self):
        upperRight = np.zeros((self.Ncoat+2,self.Ncoat+2))
        for i, mat in enumerate(self.thermophysicalProperties):
            if i == self.Ncoat: 
                upperRight[i,i] = 1
                upperRight[i,i+1] = -1
            elif i == self.Ncoat+1: continue
            # value is (a_Ncoat+1)^3 / (a_mat)^3
            value = (self.geometricalProperties[self.outerCoatingMat]['or'] / self.geometricalProperties[mat]['or'])**3
            upperRight[i,i] = value
            upperRight[i,i+1] = -1 * value
        
        return upperRight
    
    def buildLowerRightQuadrant(self):
        """
        In the NRC Document there is a typo in the matrix shown on page C-6. 
        - The negatives in the lower right quadrant should be flipped and each term
          in the diagonal of that quadrant should be multiplied by k[mat]. Similarly each
          term left of the diagonal should be multiplied by k[previous_mat]. I know this is correct
          because
           1. USNC told me it is this way
           2. If you do the example problem given in the NRC doc you get the same answer as them
              with this matrix
          """
        lowerRight = np.zeros((self.Ncoat+2,self.Ncoat+2))
        for i, mat in enumerate(self.thermophysicalProperties):
            if i == 0: 
                lowerRight[i,i] = 1
                old_mat = mat
                continue
            elif i == self.Ncoat+1:
                lowerRight[i,i] = -2 * self.thermophysicalProperties[mat]['k']
                lowerRight[i,i-1] = 2 * self.thermophysicalProperties[old_mat]['k']
                continue
            value = 2 * (self.geometricalProperties[self.outerCoatingMat]['or'] / self.geometricalProperties[old_mat]['or'])**3
            lowerRight[i,i] = -1 * value * self.thermophysicalProperties[mat]['k']
            lowerRight[i,i-1] = value * self.thermophysicalProperties[old_mat]['k']
            old_mat = mat
        return lowerRight
            
    def buildMatrix(self):
        """
        building M and b matrices in the system of equations Mx=b
        - this method comes from https://www.nrc.gov/docs/ML0909/ML090900017.pdf
        ^-- from 'Page C-6'
        - strategy is to build each quadrant of the matrix and put it together at the end
        """
        self.Ncoat = len(self.thermophysicalProperties) - 2 # minus 2 because fuel kernel + matrix mat is included
        upperLeft = self.buildUpperLeftQuadrant()
        lowerLeft = self.buildLowerLeftQuadrant()
        upperRight = self.buildUpperRightQuadrant()
        lowerRight = self.buildLowerRightQuadrant()
        
        top = np.hstack((upperLeft,upperRight))
        bottom = np.hstack((lowerLeft,lowerRight))
        
        self.M = np.vstack((top,bottom))       # m matrix mentioned above
        self.b = np.zeros(np.shape(self.M)[1]) # defining b matrix while im here
        self.b[self.Ncoat+1] = 1.0 
    
    def solveSOE(self):
        """
        solving the system of equations Mx=b mentioned in self.buildMatrix
        """
        self.x = np.linalg.solve(self.M,self.b)
    
    def get_k_p(self):
        """
        solving for the effective thermal conductivity of the triso particle
        - this method comes from https://www.nrc.gov/docs/ML0909/ML090900017.pdf
        ^-- from 'Page C-7' 
        """
        b6 = self.x[-1] # this is B_Ncoat+2
        self.keff_particle = self.thermophysicalProperties['matr']['k'] * (1 - 2*b6)/(1+b6)
    
    def get_keff(self):
        """
        solving
        """
        b6 = self.x[-1]
        product = self.geometricalProperties['alph'] * b6 #mutliplying these to shorten eqn
        self.keff_matrix = self.thermophysicalProperties['matr']['k'] * (1 - 2*product)/(1+product)
        
    def solveConductances(self):
        """
        One function that calls all of the necessary functions and just returns the final conductance
        """
        self.buildMatrix()
        self.solveSOE()
        self.get_k_p()
        self.get_keff()

if __name__ == "__main__":
    unitCell_preProcess = preProcess()
    #unitCell_preProcess.showValues()
    unitCell_preProcess.plotKappaFit()
    #unitCell_preProcess.printKappaCoefs()
    unitCell_preProcess.CpZrC() # this must be called to initialize the Cp values for ZrC
    #unitCell_preProcess.printZrCrho()
    #unitCell_preProcess.plotMuFit(True)
    unitCell_preProcess.initializeLiqH()
    #unitCell_preProcess.printLiqHValues()

    #- Triso stuff
    triso = TRISO()
    triso.solveConductances()
    print(triso.keff_matrix)

    #geom = geometry()