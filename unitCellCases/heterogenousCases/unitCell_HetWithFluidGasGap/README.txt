# =================================================================================== #
# =================================================================================== #
# ====================== Heterogenous Unit Cell (fluid gasGap) ====================== #
# ======================      -- ------------------- --      ======================== #
# =================================================================================== #
# ========================= Zach Hughes - zhughes@tamu.edu  ========================= #
# Folders:                                                                            #
# Files:                                                                              #
# 1. Allrun/Allrun.quick: These files clean the case, split the mesh, and then run    #
#    the case. They can by run by typing './Allrun' or './Allrun.quick' in the command#
#    line. Allrun.quick runs the case in parallel on 8 cores.                         #
# 2. Allclean/Allpre: These files clean the case and split the mesh into coupled      #
#    regions respectively. They can be run similarly the the Allrun/Allquick method   #
# 3. updateFiles.sh: This case has 47 regions total, making it cumbersome to update   #
#    each region independently. This file allows the user to update all regions made  #
#    out of Hydrogen(coolant channels) or porous ZrC(cladding + insulator).           #
#    instructions on how to use it are at the top of the updateFiles.sh file          #
# 4. mrf2DheatConduction.py: This is an old script that is mainly used to calculate   #
#    coolant channel boundary conditions such as inletU, epsilon, k, etc.             #
# 5. script.py:                                                                       #
#    In this script I solve the geometry of the unit cell exterior(where the hex is), #
#    the equations of state for each of the materials (besides fuel), and I made a    #
#    class solving TRISO particle effective thermal conductivity + matrix effective   #
#    thermal conductivity (although now it is a solid solution fuel)                  #
# 6. nrcThermalConductivity.py: This is the file USNC gave me that calculates TRISO   #
#    thermal conductivities. I received this after I had written it, hence two of the #
#    same.                                                                            #
# 7. unitCellFinalMesh.unv: This is the mesh of the unit cell, generated in Salome.   #
#    Unless something is deleted on accident this shouldn't be needed, but the mesh   #
#    can be generated from this using 'ideasUnvToFoam unitCellFinalMesh.unv' in the   #
#    command line                                                                     #
# ----------------------------------------------------------------------------------- #
# -- To Do:                                                                           #
#                                                                                     #
# ----------------------------------------------------------------------------------- #
# Sources:                                                                            #
# -- Hydrogen Properties                                                              #
# 1. https://webbook.nist.gov/cgi/fluid.cgi?P=3.7&TLow=80&THigh=1000&TInc=10&Digits=5&ID=C1333740&Action=Load&Type=IsoBar&TUnit=K&PUnit=MPa&DUnit=kg%2Fm3&HUnit=kJ%2Fkg&WUnit=m%2Fs&VisUnit=Pa*s&STUnit=N%2Fm&RefState=DEF
# ^ CoolProp Python API was also used to access NIST database for liqH ^              #
# -- UZrC Properties                                                                  #
# 2. https://www.sciencedirect.com/science/article/pii/S2352179122001715#s0040        #
# 3. https://www.osti.gov/biblio/4419566                                              #
# 4. ZrC kappa : https://www.osti.gov/biblio/4003326                                  #
# 5. ZrC Cp: https://www.sciencedirect.com/science/article/abs/pii/S0022311513007824  #
# =================================================================================== #
