#!/bin/bash
# ============================================================================== #
# This file was made to simplify the process of changing all of the files in     #
# each of the 47 regions. This code allows the user to change a file in one      #
# specific structure, and to copy and paste that file to all similar structures. #
# For example, system/ccc/changeDictionaryDict can be changed to edit all the    #
# coolant channel boundary conditions by setting:                                #
# copied_structure="ccc"                                                         #
# region="H"                                                                     #
# file_to_change="system/changeDictionaryDict"                                   #
# After this is set, ./updateFiles in the command line will update all of the    #
# changeDictionaryDict files in the coolant channel regions to be identical to   #
# that of the "ccc" region, edited for each particular channel                   #
# To Use:                                                                        #
# copied_structure: This is the name of the region changed by the user and that  #
#     you would like copied to all similar regions                               #
# region: Can only be "ZrC0.7" or "H"                                            #
# file_to_change: This is the folder/file that needs to be changed.              #
# ============================================================================== #
# -- REGIONS AND MATERIALS ---                                                   #
# H      -> All coolant channels, gas gap                                        #
# UZrC   -> fuelAssembly                                                         #
# ZrC    -> assemblyCan                                                          #
# ZrC0.7 -> all coolantClad, insulator                                           #
# Be     -> moderator                                                            #
# ============================================================================== #
# ---------------------------- user input options
# BE CAREFUL TO MAKE SURE INPUTS ARE CORRECT, THIS CAN BE A GREAT TOOL TO DESTROY ALL YOUR FILES
copied_structure="cccClad"
region="ZrC0.7" # 
file_to_change="system/changeDictionaryDict" # or system/fvSolutions or constant/thermophysicalProperties etc..
# -----------------------------------------------

directory=$(pwd)
folder="${file_to_change%%/*}"
directory="${directory}/${folder}" # putting system at the end of directory
file_to_change=$(basename "$file_to_change")

files=$(find "$directory" -type f -name "$file_to_change") # finding every occurance of this file in the folder

for file in $files; do
  # Get the directory name containing the file
  subdirectory=$(dirname "$file")
  subdirectory_name=$(basename "$subdirectory")
  if [[ "$region" == *"ZrC0.7"* ]]; then 
    if [[ $subdirectory_name =~ [0-9]$ ]] || [[ "$subdirectory_name" == "$copied_structure" || "$subdirectory_name" == "ccc" || "$subdirectory_name" == "moderator" || "$subdirectory_name" == "assemblyCan" || "$subdirectory_name" == "fuelAssembly" || "$subdirectory_name" == "gasGap" ]]; then # skips all coolant channel names and copied_structure
      echo "Coolant channel: $subdirectory_name was skipped bc it is not ZrC0.7"
      continue
    fi
    rm $file # removing the old file
    cp "$directory/$copied_structure/$file_to_change" "$subdirectory/$(basename "$file")" # replacing old file with fixed file
    sed -i "s/$copied_structure/$subdirectory_name/g" "$file"  # replaces every occurance of copied_structure with subdirectory name
    echo "Structure: $subdirectory_name has been updated"
  elif [[ "$region" == *"H"* ]]; then
    if [[ "${subdirectory_name: -4}" == "Clad" || "$subdirectory_name" == "fuelAssembly" || "$subdirectory_name" == "assemblyCan" ]]; then
      continue  # skipping anything that ends with "Clad" or the fuelAssembly and assemblyCan regions
    fi
    if [[ "$subdirectory_name" == "insulator" || "$subdirectory_name" == "gasGap" || "$subdirectory_name" == "moderator" || "$subdirectory_name" == "$copied_structure" ]]; then
      continue  #skipping insulator , gasGap , and moderator regions, or base(ccc) region
    fi
    rm $file # removing the old file
    cp "$directory/$copied_structure/$file_to_change" "$subdirectory/$(basename "$file")"
    sed -i "s/$copied_structure/$subdirectory_name/g" "$file"  # replaces every occurance of copied_structure with subdirectory name
    echo "Coolant Chanel: $subdirectory_name has been updated"
  fi
done


