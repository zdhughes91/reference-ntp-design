import numpy as np
import os
import pandas as pd
import matplotlib.pyplot as plt


# >>> parameters >>>
totalPower = 225 # MWt
coolChannelDiameter = 0.2 # cm
coolCladThickness = 0.025  # cm
assemblyCanThickness = 0.025 # cm
insulatorThickness = 0.2 # cm
gasGapThickness = 0.3 #cm
assemblySpacing = 2.5 # cm - spacing between assembly ODs
nCoolChannels = 91
nAssemblies = 61
fuelAssemblyDiameter = 6.35 # cm
coreActiveHeight = 110 #cm
openFoamDiskHeight = 0.0001 #m
Ts = 500 # K
#coolantInletVelocity = 47.6 # m/s
inletPressure = 7 # MPa
coreMassFlowrate = 6.33 # kg/s


rho_UZrC = 5.458e3 # based on Performance of (U,Zr)C-graphite (composite) and of (U,Zr)C (carbide) fuel elements in the Nuclear Furnace 1 test reactor
rho_liqH = 5.702806 # kg/m^3 from NIST using CoolProp (@ 300K and 7.3MPa)
Cp_UZrC = 500 # J/(kg*K) at 2500 K https://www.sciencedirect.com/science/article/pii/S2352179122001715#s0040 --> Fig.9
Cp_liqH = 14310.12686 # J/(kg*K) from NIST using CoolProp (@ 300K and 7.3MPa)
k_UZrC = 15.5 # W/(m*K)  at 1500 K https://www.sciencedirect.com/science/article/pii/S2352179122001715#s0040 --> paragraph below Fig.12
#k_UZrC = 30   # W/(m*K) at 2600 K https://www.osti.gov/servlets/purl/4419566 --> table CVIII
mu_liqH = 1.2739e-5 # Pa*s    Tis is 
k_ZrCHf = 5 # W/(m*K)   at 1200 K  https://par.nsf.gov/servlets/purl/10285629 --> Fig.7D
Cp_ZrCHf = 362 # J/kg*K), found from wikipedia for ZrC

tempFilename = os.getcwd() + f"/0.1/T"
# <<< parameters <<<
class unitCell:
    def __init__(self,assemblyRadius,canThickness,insulThickness,gasThickness,moderThickness):
        self.cm2m = 1e-2
        self.assemblyRadius = assemblyRadius * self.cm2m
        self.canThickness = canThickness * self.cm2m
        self.insulThickness = insulThickness * self.cm2m
        self.gasThickness = gasThickness * self.cm2m
        self.moderThickness = moderThickness * self.cm2m

        self.totalPower = 225e6 # W
        self.rhoUZrC = 5.458e3 # kg/m^3
        self.CpUZrC = 500 #J/kg K
    def getRadialDimensions(self):
        self.assemblyCanOR = self.assemblyRadius + self.canThickness
        self.insulatorOR = self.assemblyCanOR + self.insulThickness
        self.gasGapOR = self.insulatorOR + self.gasThickness
        self.moderatorOR = self.insulatorOR + self.moderThickness
    def printRadialDimensions(self):
        #getRadialDimensions(self)
        print('---------------- Radial Dimensions ----------------')
        print('Fuel Assembly OR:',0.03175,'m')
        print('Assembly Can OR :',np.round(self.assemblyCanOR,5),'m')
        print('Insulator OR    :',np.round(self.insulatorOR,5),'m')
        print('Gas Gap OR      :',np.round(self.gasGapOR,5),'m')
        print('Moderator OR    :',np.round(self.moderatorOR,5),'m')
        print('----------------------------------------------------')
    # 'i' functions are for internal use only
    def icircularArea(self,r): 
        return np.pi*(r**2)
    def ivolumetricARea(self):
        perimeter = 2 * np.pi * self.coolantChannelRadius
        height = 1.1
        singleCoolantChannelArea = perimeter * height
        totatCoolantChannelArea = 91 * singleCoolantChannelArea
        assemblyArea = self.icircularArea(self.assemblyRadius)
        assemblyVolume = assemblyArea * height
        print('volumetric area')
        print('including coolant channels = {:0.4f}'.format(totatCoolantChannelArea/assemblyVolume))
    def imultiRegionVAandPoro(self):
        sliceArea = 0.000697124
        sliceVolume = sliceArea * 1.1
        totalCoolantChannelArea = 21 * self.icircularArea(self.coolantChannelRadius)
        totalCoolantChannelHeatedArea = 21 * 2 * np.pi * self.coolantChannelRadius * 1.1
        totalCoolantChannelVolume = totalCoolantChannelArea * 1.1
        multiRegionVolumetricArea = totalCoolantChannelHeatedArea / sliceVolume
        print('multiRegion volumetricArea = {:0.4f} '.format(multiRegionVolumetricArea))
        porosity = (sliceVolume - totalCoolantChannelVolume) / sliceVolume
        print('multiRegion Porosity = {:0.4f} '.format(porosity))

    def iInletVelocity(self):
        # currently not using gas gap area because I am not sure if its included in the mass flowrate
        inletArea =  61 * (91 * self.icircularArea(self.coolantChannelRadius)) #+  61 * (self.icircularArea(self.gasGapOR) - self.icircularArea(self.insulatorOR)) )
        self.inletVelocity = (self.massFlowrate / (inletArea*self.rhoLiqH)) #* 0.85 # 0.85 is to adjust for developing flow
        
    def iReynolds(self,Dh):
        return self.rhoLiqH*self.inletVelocity*Dh/self.muLiqH
    def iL(self): 
        # capital L (characteristicLength) is the equivalent diameter according to 'intro to CFD:  Versteeg and Malalasekera'
        self.ccL = 0.07*self.coolantChannelDh
        self.ggL = 0.07*self.gasGapDh
    def iTurbulenceIntensity(self):
        self.ccI = 0.16 * self.ccRe**(-1/8)
        self.ggI = 0.16 * self.ggRe**(-1/8)
    def iTurbulentKineticEnergy(self):
        self.cck = (3/2) * (self.ccI*self.inletVelocity)**2
        self.ggk = (3/2) * (self.ggI*self.inletVelocity)**2
    def iTurbulentDissapationEnergy(self):
        self.ccEpsilon = (0.09**(0.75)) * (self.cck**1.5) / self.ccL 
        self.ggEpsilon = (0.09**(0.75)) * (self.ggk**1.5) / self.ggL 
    def iMolarMass(self): # g/mol
        self.mmUZrC = 238 + 91.22 + 12.011 
        self.mmZrCHf = 91.22 + 12.011 + 178.5 
        self.mmZrC07Hf = 0.7*(91.22 + 12.011) + 178.5
        self.mmliqH = 2.0
    def iAssemblySlicePower(self): # 
        fuelAssemblyArea = self.icircularArea(self.assemblyRadius) - (91 * self.icircularArea(self.coolantChannelRadius))
        self.fuelAssemblyVolume = fuelAssemblyArea * 1.1 # m^3
        self.fuelAssemblyMass = self.fuelAssemblyVolume * self.rhoUZrC #kg
        self.sliceMass = self.fuelAssemblyMass * self.sliceAreaFraction
        self.assemblyPower = (self.totalPower/61) 
        self.sliceAbsolutePower = self.assemblyPower / 6 # because i took a 60degree slice (this is not exact)
    def ifvOptionsEnthalpy(self):
        self.fvOptionsSpecificEnthalpy = self.assemblyPower / (self.fuelAssemblyMass * self.fuelAssemblyVolume)  # W/(m^3 kg)
        self.fvOptionsAbsoluteEnthalpy = self.assemblyPower / self.fuelAssemblyMass  # W/kg
    def getLaplacianfvOptionsT(self):
        "returns the temperature input for the fvOptions internal heat generation for (DT,T) laplacian"
        T = self.assemblyPower / (self.CpUZrC * self.rhoUZrC * self.fuelAssemblyVolume)
        print('-- fvOptions T (volumeMode=specific)')
        print('T =',T,'K','\n')
        Tspec = T * (self.fuelAssemblyVolume / 6) # dividing by 6 sice I took 60degree slice(not exact)
        print('-- fvOptions T (volumeMode=absolute)')
        print('T =',Tspec,'K','\n')
        return T
    def igetSliceFraction(self):
        """the center of the slice does not occur at the cartesian origin because i wanted to include the entire central coolant channel.
           The entire fuel assembly is a circle, and the slice only represents a fraction of that circle. This function calculates that fraction
           in order to accurately predict the absolute slice power for the fvOptions input. This fraction is calculated in the following way
           1. I know I took a 60 degree slice. This is 1/6 of the area of the circle if taken from the cartesian orgin, 
               and the remaining area can be added to this area
           2. The remaining area (that will be added to #1) can be split up into two rectangles and two triangles. 
                - the area of one of these rectangles and one of these triangles becomes obvious after knowing the slice vertex
                - then due to symmetry the sum of the known rectangle and triangle is multiplied by two
           3. Salome mesh/Measurements feature predicts fuel assembly slice area of 0.000697124 m^2"""
        salomeVertexLocation = [-0.00433,-0.0025] # this is the (x,y) location of the slice
        rectangleArea = self.assemblyRadius * -salomeVertexLocation[1]
        triangleArea = (1/2) * -salomeVertexLocation[0] * -salomeVertexLocation[1]
        totalExtraArea = 2 * (rectangleArea + triangleArea)

        totalFuelAssemblyArea = self.icircularArea(self.assemblyRadius)
        totalSliceArea = (totalFuelAssemblyArea * (1/6)) + totalExtraArea
        #print('calced slicearea= {:0.8f}'.format(totalSliceArea))
        #print('salome slicearea= {:0.8f}'.format(0.000697124))
        # now using salome area since its probably more exact
        self.sliceAreaFraction = 0.000697124 / totalFuelAssemblyArea
        self.exactSlicePower = self.assemblyPower*self.sliceAreaFraction
    
    def iPredictSliceAverageTemperature(self): # i dont think thi sis going to work...
        # Q = mcdeltaT
        avgT = (self.assemblyPower/6) / (self.CpUZrC * self.fuelAssemblyMass) + 300
        avgTexact = self.exactSlicePower / (self.CpUZrC * self.fuelAssemblyMass) + 300
        self.expectedAvgTemp = [avgT,avgTexact]

    def getDh(self,coolChannelRadius):
        self.coolantChannelRadius = coolChannelRadius * self.cm2m
        Pcc = (2 * np.pi * self.coolantChannelRadius) # coolant channel 
        Acc = self.icircularArea(self.coolantChannelRadius)
        self.coolantChannelDh = 4*Acc/Pcc

        Pgg = (2*np.pi*self.gasGapOR) + (2*np.pi*self.insulatorOR) # gas gap
        Agg = self.icircularArea(self.gasGapOR) - self.icircularArea(self.insulatorOR)
        self.gasGapDh = 4*Agg/Pgg

    def getRe(self,rhoLiqH,muLiqH,massFlowrate):
        self.rhoLiqH = rhoLiqH
        self.muLiqH = muLiqH
        self.massFlowrate = massFlowrate
        self.iInletVelocity()
        self.ccRe = self.iReynolds(self.coolantChannelDh)
        self.ggRe = self.iReynolds(self.gasGapDh)

    def getTurbulenceBCs(self):
        self.iL() 
        self.iTurbulenceIntensity()
        self.iTurbulentKineticEnergy()
        self.iTurbulentDissapationEnergy()

    def printMolarMasses(self):
        self.iMolarMass()
        print('---------------- Molar Masses ----------------')
        print('UZrC      = {:0.3f} g/mol '.format(self.mmUZrC))
        print('ZrC-HF    = {:0.3f} g/mol '.format(self.mmZrCHf))
        print('ZrC0.7-Hf = {:0.3f} g/mol '.format(self.mmZrC07Hf))
        print('liqH      = {:0.3f} g/mol '.format(self.mmliqH))
        print('----------------------------------------------')
    def printSlicePower(self):
        self.iAssemblySlicePower()
        self.igetSliceFraction()
        print('Slice Power ')
        print('P = {:0.4f} W/assemblySlice '.format(self.assemblyPower/6))
        print('P = {:0.4f} W/m^3 '.format(self.assemblyPower/self.fuelAssemblyVolume))
        print('m = {:0.6f} kg '.format(self.fuelAssemblyMass))
        print('Slice Fraction')
        print('Frac = {:0.6f} '.format(self.sliceAreaFraction))
        print('exactP = {:0.4f} W/assemblySlice '.format(self.exactSlicePower))
        print('sliceMass =  {:0.4f} kg '.format(self.sliceMass))
        
    def printfvOptionsEnthalpy(self):
        self.ifvOptionsEnthalpy()
        print('h_specific = {:0.5f} W/(m^3 kg) '.format(self.fvOptionsSpecificEnthalpy))
        print('h_absolute = {:0.5f} W/kg '.format(self.fvOptionsAbsoluteEnthalpy))
        self.getLaplacianfvOptionsT()
    def printExpectedAvgTemp(self):
        self.iPredictSliceAverageTemperature()
        print('T = {:0.4f} K'.format(self.expectedAvgTemp[0]))
        print('Texact = {:0.4f} K'.format(self.expectedAvgTemp[1]))
    def printOutletMassflow(self,density,velocity):
        print("--- Outlet Mass Flowrate from Results: ---")
        print("MFR = {:0.5f} kg/s ".format(density*velocity*self.icircularArea(self.assemblyRadius)))

class postProcess:
    def __init__(self,num): # num is ss case folder number
        self.timeFolderNum = num
        regions = ['ccc','FAf']
        innerRegNames = []
        for i in range(1,21,1):
            innerRegNames.append('icc'+str(i)+'f')
        regions = np.append(regions,innerRegNames) # list of region names
        self.regions = regions

    def getData(self):
        ringCdata = pd.read_csv('cccData.csv')
        ring1data = pd.read_csv('icc1Data.csv')
        ring2data = pd.read_csv('icc3Data.csv')
        ring3data = pd.read_csv('icc6Data.csv')
        ring4data = pd.read_csv('icc10Data.csv')
        ring5data = pd.read_csv('icc15Data.csv')
        pmData = pd.read_csv('axialDataPM.csv')
        structureData = pd.read_csv('structData.csv')
        pm2Data = pd.read_csv('axialWithNewVolArea.csv')
        slice2Ddata = pd.read_csv('2dslicedataForHsHmax.csv')

        self.data = {'c':ringCdata, # putting all coolant channel data into one variable
                     '1':ring1data,
                     '2':ring2data,
                     '3':ring3data,
                     '4':ring4data,
                     '5':ring5data,
                     'pm':pmData,
                     'struct':structureData,
                     'pm2':pm2Data,
                     '2Ddata':slice2Ddata}
        
        self.data['pm']['rho'] = 6.1923 + -1.63183743e-03*self.data['pm']['T'] # adding density data for porous-media since genfoam doesnt
        self.ccNames = ['c','1','2','3','4','5']
        
    def plotCCtemp(self):
        plt.figure(1,dpi=200)
        for cc in self.ccNames:
            plt.plot(self.data[cc]['arc_length'],self.data[cc]['T'][::-1],label='Ring '+cc)
        plt.plot(self.data['pm']['arc_length'],self.data['pm']['T'],label='Porous Media Orig.',linestyle='dashed')
        plt.plot(self.data['pm2']['arc_length'],self.data['pm2']['T'],label='Porous Media New',linestyle='dashed')
        plt.legend(loc='best')
        plt.title('Coolant Channel Temperatures')
        plt.ylabel('Temperature (K)')
        plt.xlabel('Height (m)')
        plt.grid(True)
        #plt.savefig('compareCoolantTemp.png')
        plt.show()

    def plotCCpres(self):

        plt.figure(2,dpi=200)
        for cc in self.ccNames:
            plt.plot(self.data[cc]['arc_length'],1e-6*self.data[cc]['p_rgh'][::-1],label='Ring '+cc)
        plt.plot(self.data['pm']['arc_length'],1e-6*self.data['pm']['p_rgh'],label='Porous Media Orig.',linestyle='dashed')
        plt.plot(self.data['pm2']['arc_length'],1e-6*self.data['pm2']['p_rgh'],label='Porous Media New',linestyle='dashed')
        plt.legend(loc='best')
        plt.title('Coolant Channel Pressures')
        plt.ylabel('Pressure (MPa)')
        plt.xlabel('Height (m)')
        plt.grid(True)
        #plt.savefig('comparePressure.png')
        plt.show()
    
    def plotCCvelo(self):

        plt.figure(3,dpi=200)
        for cc in self.ccNames:
            plt.plot(self.data[cc]['arc_length'],np.abs(self.data[cc]['U:2'][::-1]),label='Ring '+cc)
        plt.plot(self.data['pm']['arc_length'],self.data['pm']['U:2'],label='Porous Media Orig.',linestyle='dashed')
        plt.plot(self.data['pm2']['arc_length'],self.data['pm2']['U:2'],label='Porous Media New',linestyle='dashed')
        plt.legend(loc='best')
        plt.title('Coolant Channel Z-Velocity')
        plt.ylabel('Velocity (m/s)')
        plt.xlabel('Height (m)')
        plt.grid(True)
        #plt.savefig('compareVelocity.png')
        plt.show()

    def plotStructTemp(self):
        plt.figure(4,dpi=200)
        plt.plot(self.data['pm']['arc_length'],self.data['pm']['Tmax.lumpedNuclearStructure'],label='Porous Media: Tmax',linestyle='dashed')
        plt.plot(self.data['pm']['arc_length'],self.data['pm']['Tsurface.lumpedNuclearStructure'],label='Porous Media: Tsurf',linestyle='dashed')
        plt.plot(self.data['struct']['arc_length'],self.data['struct']['T'][::-1],label='multiRegion OD')
        plt.plot(self.data['pm2']['arc_length'],self.data['pm2']['Tmax.lumpedNuclearStructure'],label='Porous Media New: Tmax',linestyle='dashed')
        plt.plot(self.data['pm2']['arc_length'],self.data['pm2']['Tsurface.lumpedNuclearStructure'],label='Porous Media New: Tsurf',linestyle='dashed')
        plt.legend(loc='best')
        plt.title('Fuel Assembly Structure Temperature')
        plt.ylabel('Temperature (K)')
        plt.xlabel('Height (m)')
        plt.grid(True)
        #plt.savefig('compareStructure.png')
        plt.show()

    def plotDensity(self):
        plt.figure(5,dpi=200)
        for cc in self.ccNames:
            plt.plot(self.data[cc]['arc_length'],np.abs(self.data[cc]['rho'][::-1]),label='Ring '+cc)
        plt.plot(self.data['pm']['arc_length'],self.data['pm']['rho'],label='Porous Media Orig.',linestyle='dashed')
        plt.plot(self.data['pm2']['arc_length'],self.data['pm2']['rho'],label='Porous Media New',linestyle='dashed')
        plt.legend(loc='best')
        plt.title('Coolant Channel Densities')
        plt.ylabel('Density (kg/m^3)')
        plt.xlabel('Height (m)')
        plt.grid(True)
        #plt.savefig('compareDensity.png')
        plt.show()

    def plotMassFlowrate(self):
        channelArea = np.pi * (0.001**2)
        channelMFR = 6.33 / (91 * 61)
        plt.figure(3,dpi=200)
        for cc in self.ccNames:
            mfr_channel = np.abs(self.data[cc]['U:2'][::-1]) * np.abs(self.data[cc]['rho'][::-1]) * channelArea
            plt.plot(self.data[cc]['arc_length'],mfr_channel,label='Ring '+cc)
        plt.plot(self.data['pm']['arc_length'],self.data['pm']['U:2']*self.data['pm']['rho']*np.pi*(0.001**2),label='Porous Media',linestyle='dashed')
        plt.legend(loc='best')
        plt.axhline(channelMFR,label='Expected MFR')
        plt.title('Coolant Channel Mass Flowrate')
        plt.ylabel('Mass Flowrate (kg/s)')
        plt.xlabel('Height (m)')
        plt.grid(True)
        plt.savefig('compareMassFlowrate.png')
        plt.show()
    
    def heatConductanceEqn(self,power,T1,T2):
        return (power/(T2-T1))
    
    def findHeatConductances(self):
        specificPower = 1.16e9 # W/m^3
        maxT,avgT,surT = np.max(self.data['2Ddata']['T']), np.mean(self.data['2Ddata']['T']), np.min(self.data['2Ddata']['T'])
        H_maxToAvg = self.heatConductanceEqn(specificPower,avgT,maxT)
        H_avgToSur = self.heatConductanceEqn(specificPower,surT,avgT)
        print('H_maxToAvg = {:0.3f} W/ '.format(H_maxToAvg*1e-6))
        print('H_avgToSur = {:0.3f} W/ '.format(H_avgToSur*1e-6))

    def plotAll(self):
        self.plotCCtemp()
        self.plotCCpres()
        self.plotCCvelo()
        self.plotStructTemp()
        self.plotDensity()
        #self.plotMassFlowrate()



if __name__ == '__main__':
    unit = unitCell(fuelAssemblyDiameter/2,assemblyCanThickness,insulatorThickness,gasGapThickness,assemblySpacing/2)
    unit.getRadialDimensions(), unit.printRadialDimensions()
    unit.getDh(coolChannelDiameter/2)#, print('dhgg = {:.4f} m'.format(unit.gasGapDh))


    unit.getRe(rho_liqH,mu_liqH,coreMassFlowrate)

    print('Re_cc = {:0.4f} '.format(unit.ccRe))
    print('Re_gg = {:0.4f} '.format(unit.ggRe))
    print('Uinlet = {:0.4f} m/s'.format(unit.inletVelocity))

    unit.getTurbulenceBCs()
    print('Lcc,Lgg = {:0.6f},{:0.6f}'.format(unit.ccL,unit.ggL))
    print('Icc,Igg = {:0.4f},{:0.4f}'.format(unit.ccI,unit.ggI))
    print('kcc,kgg = {:0.4f},{:0.4f}'.format(unit.cck,unit.ggk))
    print('Epcc,Epgg = {:0.4f},{:0.4f}'.format(unit.ccEpsilon,unit.ggEpsilon))

    #unit.printMolarMasses()
    #unit.printSlicePower()
    #unit.printfvOptionsEnthalpy()
    unit.ivolumetricARea()
    unit.imultiRegionVAandPoro()
    #unit.printExpectedAvgTemp()

    # post processing
    pp = postProcess(0.6)
    pp.getData()
    #pp.plotAll()
    pp.findHeatConductances()
    #pp.plotMassFlowrate()
    #pp.printMaxTs()
    #print('vol',pp.fuelAssemblyVolume)