#!/bin/bash
# --------------------------------------------- #
# purpose of this is to copy the ccc file and   #
# paste to each other coolant channel folder    #
# while changing the appropriate boundaries     #
# --------------------------------------------- #
# --- user input options
# base decomposeParDict:
base_cdd="ccc"
# Directory where the files are located
directory="/home/zhughes/projects/GeN-Foam/usnc/multiRegionCaseS/system"





# Find all files named 'decomposeParDict' within the directory and its subdirectories
files=$(find "$directory" -type f -name 'decomposeParDict')

subdirectories=()
for file in $files; do
  # Get the directory name containing the file
  subdirectory=$(dirname "$file")
  subdirectory_name=$(basename "$subdirectory")
  if [[ "$subdirectory_name" == "$base_cdd" ]]; then # i dont want to loop thru the decomposeParDict im editing
    continue
  fi 
  #if [[ "$subdirectory_name" == "FAf" ]]; then # i dont want to loop thru the decomposeParDict of solid
  #  continue
  #fi 
  #if [[ "$subdirectory_name" == "system" ]]; then # i dont want to loop thru the decomposeParDict of solid
  # continue
  #fi 
  
  rm $file # removing the old decomposeParDict
  cp "$directory/$base_cdd/decomposeParDict" "$subdirectory/$(basename "$file")" # pasting new base_cdd decomposeParDict

  sed -i "s/$base_cdd/$subdirectory_name/g" "$file"  # replaces every occurance of base_cdd with subdirectory name
  echo "Coolant Chanel: $subdirectory_name has been updated"
  
  #if [[ "$subdirectory_name" == "icc17f" ]]; then
  #  break
  #fi

done

