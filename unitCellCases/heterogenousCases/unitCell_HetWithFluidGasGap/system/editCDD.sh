#!/bin/bash
# --------------------------------------------- #
# purpose of this is to copy the ccc file and   #
# paste to each other coolant channel folder    #
# while changing the appropriate boundaries     #
# --------------------------------------------- #
# --- user input options
# base changeDictionaryDict:
base_cdd="ccc"
# Directory where the files are located
directory="/home/zhughes/projects/GeN-Foam/usnc/cladMultiRegion/system"





# Find all files named 'changeDictionaryDict' within the directory and its subdirectories
files=$(find "$directory" -type f -name 'changeDictionaryDict')

subdirectories=()
for file in $files; do
  # Get the directory name containing the file
  subdirectory=$(dirname "$file")
  subdirectory_name=$(basename "$subdirectory")
  if [[ "$subdirectory_name" == "$base_cdd" ]]; then # i dont want to loop thru the changeDictionaryDict im editing
    continue
  fi 
  if [[ "$subdirectory_name" == "FA" ]]; then # i dont want to loop thru the changeDictionaryDict of solid
    continue
  fi 
  if [[ "${subdirectory_name: -4}" == "Clad" ]]; then
    continue  # If it ends with 'Clad', skip to the next iteration (if used within a loop)
  fi
  #echo $subdirectory_name #prints coolant channel name
  #echo $file #prints full file path
  
  rm $file # removing the old changeDictionaryDict
  cp "$directory/$base_cdd/changeDictionaryDict" "$subdirectory/$(basename "$file")" # pasting new base_cdd changeDictionaryDict

  sed -i "s/$base_cdd/$subdirectory_name/g" "$file"  # replaces every occurance of base_cdd with subdirectory name
  echo "Coolant Chanel: $subdirectory_name has been updated"
  
  #if [[ "$subdirectory_name" == "icc17f" ]]; then
  #  break
  #fi

done

