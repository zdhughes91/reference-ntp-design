/*--------------------------------*- C++ -*----------------------------------*\
| =========                 |                                                 |
| \\      /  F ield         | OpenFOAM: The Open Source CFD Toolbox           |
|  \\    /   O peration     | Version:  v2212                                 |
|   \\  /    A nd           | Website:  www.openfoam.com                      |
|    \\/     M anipulation  |                                                 |
\*---------------------------------------------------------------------------*/
FoamFile
{
    version     2.0;
    format      ascii;
    class       dictionary;
    object      changeDictionaryDict;
}
// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

U
{
    internalField   uniform (0.01 0 0);

    boundaryField
    {
    gasGap
    {
        type            fixedValue;
        value           uniform (0 0 0);
    }
    gasGap_top
    {
        type            fixedValue;
        value           uniform (0 0 0);
    }
    gasGap_to_insulator
    {
        type            fixedValue;
        value           uniform (0 0 0);
    }
    gasGap_to_moderator
    {
        type            fixedValue;
        value           uniform (0 0 0);
    }
    ".*"
    {
        type            zeroGradient;
    }
    }
}

T
{
    internalField   uniform 300;

    boundaryField
    {
        gasGap_to_insulator
        {
            type            compressible::turbulentTemperatureRadCoupledMixed;
            Tnbr            T;
            kappaMethod     fluidThermo;
            value           uniform 300;
        }
        gasGap_to_moderator
        {
            type            compressible::turbulentTemperatureRadCoupledMixed;
            Tnbr            T;
            kappaMethod     fluidThermo;
            value           uniform 300;
        }
        ".*"
        {
            type            zeroGradient;
        }
    }
}

epsilon
{
    internalField   uniform 0.01;
    boundaryField
    {
        ".*"
        {
            type            epsilonWallFunction;
            value           uniform 0.01;
        }
        gasGap_walls
        {
            type            zeroGradient;
            value           uniform 0.01;
        }
    }
}

k
{
    internalField   uniform 0.01;
    boundaryField
    {
        ".*"
        {
            type            kqRWallFunction;
            value           uniform 0.01;
        }
        gasGap_walls
        {
            type            zeroGradient;
            value           uniform 0.01;
        }
    }
}

p_rgh
{
    internalField   uniform 8e6;

    boundaryField
    {
        ".*" // this may be more stable as fixedfluxpressure...
        {
            type            fixedFluxPressure;
            value           uniform 8e6;
        }
    }
}

p
{
    internalField   uniform 8e6;

    boundaryField
    {
        ".*"
        {
            type            calculated;
            value           uniform 8e6;
        }
    }
}

nut
{
    internalField   uniform 0.01;

    boundaryField
    {
        ".*"
        {
            type            calculated;
            value           uniform 0.01;
        }
    }
}

alphat
{
    internalField   uniform 0.01;

    boundaryField
    {
        ".*"
        {
            type            calculated;
            value           uniform 0.01;
        }
    }
}
// ************************************************************************* //
