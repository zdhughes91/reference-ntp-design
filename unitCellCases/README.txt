# =================================================================================== #
# =================================================================================== #
# ====================== Reference NTP Design Unit Cell Model ======================= #
# ======================      -- First Working Model --      ======================== #
# =================================================================================== #
# ========================= Zach Hughes - zhughes@tamu.edu  ========================= #
# Folders:                                                                            #
# 1. heterogenousCases: This folder contains all heterogenous cases of the unit cell. #
#    They are all solved using 'chtMultiRegionSimpleFoam', and two models exist. The  #
#    first treats the gas gap region as a solid and runs without problems, and the    #
#    second treats the gas gap region as a fluid and is not yet working properly.     #
# 2. hybridCases: These contain the porous-media + heterogenous hybrid cases. These   #
#    are solved using 'GeN-Foam', and neither of them work. It is my understanding    #
#    that development in GeN-Foam in the near future will add capabilities to make    #
#    them work. The first case treats the assembly can as a solid region with coupled #
#    boundary conditions and the second treats the assembly can as the same structure #
#    only separated by a baffle. These models only have the assembly can and not the  #
#    whole unit cell because it is easier to start with fewer moving parts.           #
# ----------------------------------------------------------------------------------- #
# -- To Do:                                                                           #
# 1. Add capabilities to GeN-Foam so full unit cell can be modeled.                   #
# 2. Figure out why it is so hard to treat gas gap as a fluid region                  #
#                                                                                     #
# ----------------------------------------------------------------------------------- #
# Sources:                                                                            #
# -- Hydrogen Properties                                                              #
# 1. https://webbook.nist.gov/cgi/fluid.cgi?P=3.7&TLow=80&THigh=1000&TInc=10&Digits=5&ID=C1333740&Action=Load&Type=IsoBar&TUnit=K&PUnit=MPa&DUnit=kg%2Fm3&HUnit=kJ%2Fkg&WUnit=m%2Fs&VisUnit=Pa*s&STUnit=N%2Fm&RefState=DEF
# ^ CoolProp Python API was also used to access NIST database for liqH ^              #
# -- UZrC Properties                                                                  #
# 2. https://www.sciencedirect.com/science/article/pii/S2352179122001715#s0040        #
# 3. https://www.osti.gov/biblio/4419566                                              #
# 4. ZrC kappa : https://www.osti.gov/biblio/4003326                                  #
# 5. ZrC Cp: https://www.sciencedirect.com/science/article/abs/pii/S0022311513007824  #
# =================================================================================== #
